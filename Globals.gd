extends Node

signal heat_changed(new_heat)

var pause_on_blur := false
var _palette: Gradient = preload("res://effects/palette_gradient.tres")
var heat := 0 setget set_heat
var nb_slashes := 0
var nb_misses := 0
var is_ending := false
var bg_tex := ImageTexture.new() # color texture painted instead of black bars

onready var dialogue_box: DialogueBox = $HUD/Dialogue
onready var music: MidiPlayer = $Music
onready var trans_anim: AnimationPlayer = $HUD/Transition/Anim
onready var colorizer: CanvasItem = $Overlay/Buffer/Colorizer
onready var crt: CanvasItem = $Overlay/CRTEffect
onready var pause_menu: CanvasItem = $Overlay/PauseMenu
onready var menu_node: CanvasItem = $HUD/Menu
onready var sound_hover: AudioStreamPlayer2D = $ButtonHover


func _ready():
	# Setup colorizer
	set_heat(0)
	var rid := bg_tex.get_rid()
	VisualServer.black_bars_set_images(rid, rid, rid, rid)

	get_viewport().connect("size_changed", self, "_root_viewport_size_changed")
	get_viewport().size = Vector2(320, 180)
	if not OS.has_feature("web"):
		var zoom := floor(min(OS.get_screen_size().x / 320, OS.get_screen_size().y / 180))
		OS.window_size = Vector2(320, 180) * zoom


func _root_viewport_size_changed():
	get_viewport().size = OS.window_size if crt.visible else Vector2(320, 180)


func play_music(filename: String, loop_point := 0.0):
	# TODO: Loop point is a magic number (not s/ms/beats so what is it?)
	music.stop()
	yield(get_tree(), "idle_frame")
	music.file = filename
	music.volume_db = -80
	if filename:
		music.volume_db = -15
		music.play()
		music.loop_start = loop_point


func set_ticket(mode := "normal"):
	assert(mode in ["normal", "art", "bloody"])
	var tex: AtlasTexture = menu_node.texture_normal
	match mode:
		"normal":
			tex.region.position = Vector2(0, 0)
		"art":
			tex.region.position = Vector2(48, 24)
		"bloody":
			tex.region.position = Vector2(72, 24)
		_:
			assert(false, "Mode not found")
	menu_node.texture_normal = tex

func set_heat(new_heat: int):
	emit_signal("heat_changed", heat, new_heat)
	heat = new_heat
	_palette.colors[0].h = 0.6 + new_heat / 300.0
	_palette.colors[1].h = 0.2 - new_heat / 1500.0
	update_margins() # set border color


func update_margins():
	var img := Image.new()
	img.create(4, 4, true, Image.FORMAT_RGB8)
	img.fill(_palette.colors[0] if Globals.colorizer.visible else Color.black)
	bg_tex.create_from_image(img)


func transition() -> GDScriptFunctionState:
	dialogue_box.set_chatter()
	trans_anim.play("to_black")
	yield(trans_anim, "animation_finished")
	trans_anim.play("from_black")
	return


func _on_Menu_pressed():
	pause_menu.show()


func add_to_backlog(line: Line):
	var backlog: Label = pause_menu.backlog
	backlog.text += "\n" + tr(line.message).replace("\n", " ")


func add_speaker_to_backlog(speaker: Speaker):
	var backlog: Label = pause_menu.backlog
	if speaker and speaker.name:
		backlog.text += "\n\n" + tr(speaker.name) + ":"
	else:
		backlog.text += "\n"


func _notification(what: int):
	# When focusing out, stop music and pause
	if not pause_on_blur:
		return
	if what == MainLoop.NOTIFICATION_WM_FOCUS_OUT:
		AudioServer.set_bus_mute(0, true)
		if music.playing:
			music.stop()
		_on_Menu_pressed()
	elif what == MainLoop.NOTIFICATION_WM_FOCUS_IN:
		pass
		AudioServer.set_bus_mute(0, false)
		if music.file:
			music.playing = true


func play_hover(at_global_pos: Vector2):
	sound_hover.global_position = at_global_pos
	sound_hover.pitch_scale = 2.5 - at_global_pos.y / 200.0
	sound_hover.play()
