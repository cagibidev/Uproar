# Uproar 🦗🏛️🐜

A small point-and-click visual-novel adventure in Bug Parliament.

The first version was made in one week for the [Godot Wild Jam #53](https://itch.io/jam/godot-wild-jam-53/rate/1889105), the theme of which was "Assembly Required". It ranked 3rd out of 175 participants.

- [Game page on itch.io](https://cagibi.itch.io/uproar)
- [Android version on Play Store](https://play.google.com/store/apps/details?id=org.cagibidev.uproar) (less frequent updates)

---

### Tools used

- Code: Godot Engine 3.x
- Audio: Polyphone, Beepbox, LMMS, Audacity
- Graphics: Aseprite

### Credits

- Fonts from the [Humblefonts Gold Pack](https://somepx.itch.io/humble-fonts-gold)
- Water shader derived from [NekotoArts](https://godotshaders.com/author/nekotoarts/)'
- Diamond transition shader from [mackatap](https://godotshaders.com/author/mackatap/)
- CRT shader [from pend00 on Godot Shaders](https://godotshaders.com/shader/VHS-and-CRT-monitor-effect/)
- Audio
  - Instruments: see [soundfont](music/uproar.sf2)'s description
  - Scan & Launch sfx: CC0, rubberduck
  - Typing sounds: [CC-BY 3.0](http://creativecommons.org/licenses/by/3.0/) [eklee](http://freesound.org/people/eklee/) & [qubodup](http://freesound.org/people/qubodup/)
  - Crowd chatter & broken glass from _Phoenix Wright: Ace Attorney_, by Capcom (TODO: replace)
  - [Godot MIDI Player by Yui Kinomoto @arlez80](https://bitbucket.org/arlez80/godot-midi-player/)

---

Uproar's source code is licensed under [the MIT License](LICENSE-code).  
Game art is licensed under a [Creative Commons Attribution 4.0 International License](LICENSE-art).

By cagibidev, 2023-2024
