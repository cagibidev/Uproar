extends "res://bullets/Board.gd"


# all are diagonals here

func setup_dialogues():
	music = "res://music/argue_high.mid"
	positions = [0,1,2,3,0,1,2,3,0,3,2,1,3,1,2,0]
	statement = "B_ASSAULT_16_WORDS"

func dialogue_start() -> GDScriptFunctionState:
	yield(scara.say("B_ASSAULT_1"), "completed")
	yield(scara.say("B_ASSAULT_2"), "completed")
	yield(scara.say("B_ASSAULT_3"), "completed")
	yield(scara.say("B_ASSAULT_4"), "completed")
	return yield(scara.say(statement), "completed")
