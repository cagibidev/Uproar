extends Node2D

signal completed
signal no_bullet_left

export var points := 0 setget set_points
export var threshold := 8 setget set_threshold
export var bullet_scene: PackedScene = preload("res://bullets/Word.tscn")
var max_points := 10
var nb_bullets := 0

export var delay := 0.5
var positions := []
var statement := ""
var music := "res://music/argue_low.mid"
var scara := Speaker.new("N_SCARA")

onready var spawn_points := $SpawnPoints
onready var splats := $Results
onready var lose_sound: AudioStreamPlayer = $Loss
onready var win_sound: AudioStreamPlayer = $Win/Sound

# dialogue lines
func dialogue_start() -> GDScriptFunctionState:
	assert(false, "Override this!")
	return yield(get_tree(), "idle_frame")
func dialogue_try_again() -> GDScriptFunctionState:
	yield(scara.think("ARGUE_FAIL1_SCARA"), "completed")
	return yield(scara.say("ARGUE_FAIL2_SCARA"), "completed")
func dialogue_results() -> GDScriptFunctionState:
	return yield(Globals.dialogue_box.narrate("ARGUE_WIN1"), "completed")
func dialogue_perfect() -> GDScriptFunctionState:
	return yield(Globals.dialogue_box.narrate("ARGUE_PERFECT1"), "completed")


func set_points(new_points: int):
	points = new_points
	if not is_inside_tree():
		return
	if points >= threshold:
		$Win.show()
		$Win.scale = Vector2(2, 2)
		create_tween().tween_property($Win, "scale", Vector2.ONE, 0.2)
	else:
		$Win.hide()
	$Points.text = str(points)
	for splat_id in splats.get_child_count():
		splats.get_child(splat_id).visible = points > splat_id


func set_threshold(new_t: int):
	threshold = new_t
	$Threshold.text = str(threshold)


func setup_dialogues():
	pass # override this!


func _ready():
	set_points(0)

	setup_dialogues()
	if music:
		Globals.play_music(music)
	yield(dialogue_start(), "completed")
	try()


func try():
	start_bullet_sequence()
	yield(self, "no_bullet_left")

	if points >= threshold:
		win()
	else:
		yield(lose(), "completed")
		try()


func lose() -> GDScriptFunctionState:
	for _i in range(3):
		lose_sound.play()
		yield(lose_sound, "finished")
	return yield(dialogue_try_again(), "completed")


func win():
	if music:
		Globals.play_music("")
	var is_perfect := points == max_points
	var win_pitches := [1.33, 1.5, 1.5] if is_perfect else [1]

	for pitch in win_pitches:
		win_sound.pitch_scale = pitch
		win_sound.play()
		yield(win_sound, "finished")
	yield(dialogue_perfect() if is_perfect else dialogue_results(), "completed")
	emit_signal("completed")


func start_bullet_sequence():
	set_points(0)
	max_points = len(positions)
	var statement_array := tr(statement).replace("\n", " ").split(" ")

	nb_bullets = max_points
	for i in max_points:
		var anchor: Node2D = spawn_points.get_child(positions[i])
		var bullet := bullet_scene.instance()
		bullet.set_word(statement_array[i])
		bullet.velocity = Vector2(rand_range(-10, 10), -200).rotated(anchor.rotation)
		bullet.position = anchor.global_position
		add_child(bullet)
		bullet.connect("hit", self, "_on_bullet_hit", [bullet])
		bullet.connect("tree_exited", self, "_on_bullet_destroyed")

		yield(get_tree().create_timer(delay, false), "timeout")


func _on_bullet_hit(bullet: Node):
	var slash := preload("res://bullets/BulletBlast.tscn").instance()
	slash.position = bullet.position
	add_child(slash)
	bullet.queue_free()
	set_points(points + 1)
	if points >= threshold:
		var sfx: AudioStreamPlayer2D = slash.get_node("Sound")
		sfx.pitch_scale = min(2, 1 + (points - threshold + 1)/20.0)
		sfx.volume_db += 3


func _on_bullet_destroyed():
	nb_bullets -= 1
	if nb_bullets <= 0:
		emit_signal("no_bullet_left")
