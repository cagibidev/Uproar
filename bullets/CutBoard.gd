extends "res://bullets/Board.gd"

# 2&3 are upside down!

func setup_dialogues():
	music = "res://music/argue_high.mid"
	positions = [0,1,2,3,0,1,3,2,0,1,0,2,3,2]
	statement = "B_CUT_14_WORDS"

func dialogue_start() -> GDScriptFunctionState:
	yield(scara.think("B_CUT_1"), "completed")
	yield(scara.say("B_CUT_2"), "completed")
	yield(scara.say("B_CUT_3"), "completed")
	yield(scara.say("B_CUT_4"), "completed")
	return yield(scara.say(statement), "completed")
