extends "res://bullets/Board.gd"


func setup_dialogues():
	positions = [3,2,1,0,3,2,1,2,1,0,3,0]
	statement = "B_FLY_12_WORDS"

func dialogue_start() -> GDScriptFunctionState:
	yield(scara.say("B_FLY_1"), "completed")
	yield(scara.say("B_FLY_2"), "completed")
	yield(scara.say("B_FLY_3"), "completed")
	return yield(scara.say(statement), "completed")
