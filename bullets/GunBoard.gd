extends "res://bullets/Board.gd"


# all are sideways here

func setup_dialogues():
	music = "res://music/argue_high.mid"
	positions = [0,1,0,1,2,3,2,3,0,1,2,3]
	statement = "B_GUN_12_WORDS"

func dialogue_start() -> GDScriptFunctionState:
	yield(scara.say("B_GUN_1"), "completed")
	yield(scara.say("B_GUN_2"), "completed")
	yield(scara.say("B_GUN_3"), "completed")
	yield(scara.say("B_GUN_4"), "completed")
	return yield(scara.say(statement), "completed")
