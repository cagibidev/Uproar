extends "res://bullets/Board.gd"


func setup_dialogues():
	positions = [1,1,1,3,0,0,0,3,2,2,2]
	statement = "B_SEARCH_11_WORDS"

func dialogue_start() -> GDScriptFunctionState:
	yield(scara.say("B_SEARCH_1"), "completed")
	yield(scara.say("B_SEARCH_2"), "completed")
	yield(scara.say("B_SEARCH_3"), "completed")
	yield(scara.say("B_SEARCH_4"), "completed")
	return yield(scara.say(statement), "completed")
