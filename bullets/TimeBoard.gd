extends "res://bullets/Board.gd"


func setup_dialogues():
	positions = [3,0,3,0,1,2,2,1]
	statement = "B_TIME_8_WORDS"

func dialogue_start() -> GDScriptFunctionState:
	yield(scara.say("B_TIME_1"), "completed")
	yield(scara.say("B_TIME_2"), "completed")
	yield(scara.say("B_TIME_3"), "completed")
	yield(scara.say("B_TIME_4"), "completed")
	yield(scara.say("B_TIME_5"), "completed")
	yield(scara.say("B_TIME_6"), "completed")
	return yield(scara.say(statement), "completed")
