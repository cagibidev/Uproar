extends "res://bullets/Board.gd"


var bernard := Speaker.new("N_BERNARD").with_bleep(Speaker.Bleep.Low)


func dialogue_start() -> GDScriptFunctionState:
	yield(bernard.say("TUTO_1_BERNARD"), "completed")
	yield(bernard.say("TUTO_2_BERNARD"), "completed")
	yield(scara.say("TUTO_3_SCARA"), "completed")
	return yield(Globals.dialogue_box.narrate("TUTO_4"), "completed")
func dialogue_try_again() -> GDScriptFunctionState:
	yield(bernard.say("TUTO_FAIL1_BERNARD"), "completed")
	return yield(Globals.dialogue_box.narrate("TUTO_4"), "completed")

func common_end() -> GDScriptFunctionState:
	Globals.set_ticket("art")
	var res: GDScriptFunctionState = yield(Globals.dialogue_box.narrate("TUTO_WIN2"), "completed")
	Globals.play_music("res://music/city.mid", 3066)
	return res
func dialogue_results() -> GDScriptFunctionState:
	yield(bernard.say("TUTO_WIN1_BERNARD"), "completed")
	return yield(common_end(), "completed")
func dialogue_perfect() -> GDScriptFunctionState:
	yield(bernard.say("TUTO_PERFECT1_BERNARD"), "completed")
	return yield(common_end(), "completed")


func setup_dialogues():
	music = "res://music/tuto.mid"
	positions = [0,1,2,3,2,1,0,1,2,3]
	statement = "TUTO_10_WORDS"
