extends Node2D

signal hit

export var velocity := Vector2()
export var acceleration := Vector2(0, 200)
var is_slashed := false


func set_word(word: String):
	$Label.text = word


func _physics_process(delta: float):
	rotation += 5 * delta
	velocity += acceleration * delta
	position += velocity * delta


func _on_VisibilityNotifier2D_screen_exited():
	if not is_slashed:
		Globals.nb_misses += 1
	queue_free()


func _on_Shape_gui_input(event: InputEvent):
	if event is InputEventMouseButton and event.is_pressed():
		hit()


func hit():
	Globals.nb_slashes += 1
	is_slashed = true
	emit_signal("hit")
