extends "res://bullets/boss/BossBoard.gd"


# Argument of authority
func setup_dialogues():
	.setup_dialogues()
	positions = [0,1,0,3,2,3,0,1,0,3,2,3,0,3,0,3,0,2,1] # 19
	statement = "W E A R E T H E O N E S I N P O W E R"

func dialogue_start() -> GDScriptFunctionState:
	yield(Globals.dialogue_box.narrate("B_AUTH_1"), "completed")
	yield(forminister.say("B_AUTH_2_BOSS"), "completed")
	yield(scara.say("B_AUTH_3_SCARA"), "completed")
	yield(forminister.say("B_AUTH_4_BOSS"), "completed")
	return yield(forminister.say("B_AUTH_5_BOSS"), "completed")
