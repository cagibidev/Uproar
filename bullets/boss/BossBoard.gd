extends "res://bullets/Board.gd"

signal lost


var forminister := Speaker.new("N_FORMINISTER").with_bleep(Speaker.Bleep.VeryLow).with_talk_speed(2)
var losses := [
	"BOSS_FAIL1A",
	"BOSS_FAIL1B",
	"BOSS_FAIL1C",
]

# Strawman fallacy
func dialogue_start() -> GDScriptFunctionState:
	yield(Globals.dialogue_box.narrate("B_STRAW_1"), "completed")
	yield(forminister.say("B_STRAW_2_BOSS"), "completed")
	yield(scara.say("B_STRAW_3_SCARA"), "completed")
	yield(forminister.say("B_STRAW_4_BOSS"), "completed")
	return yield(forminister.say("B_STRAW_5_BOSS"), "completed")
func dialogue_try_again() -> GDScriptFunctionState:
	yield(forminister.say(losses[randi()%3]), "completed")
	return yield(Globals.dialogue_box.narrate("BOSS_FAIL2"), "completed")
func dialogue_results() -> GDScriptFunctionState:
	return yield(scara.say("BOSS_WIN"), "completed")
func dialogue_perfect() -> GDScriptFunctionState:
	return yield(scara.say("BOSS_PERFECT"), "completed")


func setup_dialogues():
	music = ""
	positions = [0,1,2,3,2,1,0,1,2,3,2,1,0,1,2,3] # 16
	statement = "P O O R U N A C C E P T A B L E"


func lose():
	var a = yield(.lose(), "completed")
	emit_signal("lost")
	return a


func _on_Anim_animation_finished(anim_name: String):
	if anim_name == "boss_start":
		$Anim.play("boss_continuous")
