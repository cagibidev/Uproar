extends "res://bullets/boss/BossBoard.gd"

# warning-ignore:unused_signal
signal pun


func setup_dialogues():
	.setup_dialogues()
	positions = []
	for i in 25:
		positions.append_array([0,1,2,3])
	statement = "A ".repeat(100)

func dialogue_start() -> GDScriptFunctionState:
	return yield(forminister.say("FAKE_END_1"), "completed")

func dialogue_results() -> GDScriptFunctionState:
	Globals.play_music("")
	yield(Globals.dialogue_box.narrate("FAKE_END_2"), "completed")
	emit_signal("pun")
	$Anim.stop()
	return yield(Globals.dialogue_box.narrate("FAKE_END_3"), "completed")

func dialogue_perfect() -> GDScriptFunctionState:
	emit_signal("pun")
	$Anim.stop()
	return yield(Globals.dialogue_box.narrate("FAKE_END_4"), "completed")
