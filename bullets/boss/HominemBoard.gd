extends "res://bullets/boss/BossBoard.gd"


# Argument of authority
func setup_dialogues():
	.setup_dialogues()
	positions = [0,1,3,2,0,1,3,2,0,1,2,3,2,1,0,1,3] # 17
	statement = "Y O U C A N N O T B E W O R T H Y"

func dialogue_start() -> GDScriptFunctionState:
	yield(Globals.dialogue_box.narrate("B_HOMINEM_1"), "completed")
	yield(forminister.say("B_HOMINEM_2_BOSS"), "completed")
	yield(forminister.say("B_HOMINEM_3_BOSS"), "completed")
	yield(forminister.say("B_HOMINEM_4_BOSS"), "completed")
	yield(forminister.say("B_HOMINEM_5_BOSS"), "completed")
	yield(scara.say("B_HOMINEM_6_SCARA"), "completed")
	yield(scara.say("B_HOMINEM_7_SCARA"), "completed")
	yield(scara.say("B_HOMINEM_8_SCARA"), "completed")
	return yield(forminister.say("B_HOMINEM_9_BOSS"), "completed")
