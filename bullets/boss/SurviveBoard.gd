extends "res://bullets/boss/BossBoard.gd"


# Survivorship Bias
func setup_dialogues():
	.setup_dialogues()
	positions = [0,1,2,1,2,3,0,1,2,1,2,3,2,1] # 14
	statement = "W O R K T I L L Y O U D I E"

func dialogue_start() -> GDScriptFunctionState:
	yield(Globals.dialogue_box.narrate("B_SURVIVE_1"), "completed")
	yield(forminister.say("B_SURVIVE_2_BOSS"), "completed")
	yield(scara.say("B_SURVIVE_3_SCARA"), "completed")
	yield(scara.say("B_SURVIVE_4_SCARA"), "completed")
	return yield(forminister.say("B_SURVIVE_5_BOSS"), "completed")
