extends Sprite
class_name Character


enum Mood { NEUTRAL, LOOKAWAY, THINK, HAPPY, SHOCK, SAD, ANGRY }

var speaker: Speaker
onready var anim_node: AnimationPlayer = $Anim


func _ready():
	$Shadow.scale.x = ((region_rect.size.x if region_enabled else texture.get_size().x) / hframes / 40.0)
	Globals.dialogue_box.connect("speaker_changed", self, "_speaker_changed")


func _speaker_changed(new_speaker: Speaker):
	var new_name := new_speaker.name if new_speaker != null else ""
	if speaker and new_name == speaker.name:
		anim_node.play("normal" if Globals.heat > 50 else "fast")
	else:
		anim_node.play("fast" if Globals.heat > 50 else "normal")
		anim_node.playback_speed = (rand_range(0.9, 1.1) if Globals.heat > 50 else 1.0)

	if speaker and speaker.party == Speaker.Party.GRASSHOPPER and new_name == "N_ALLG":
		anim_node.play("jump")


func flip():
	scale.x = 0
	create_tween().tween_property(self, "scale:x", 1.0, 0.2).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)
