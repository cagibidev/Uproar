extends Resource
class_name Speaker

enum Bleep { High, Mid, Low, VeryLow, Type }
enum Party { INDEPENDENT, ANT, GRASSHOPPER, TERMITE }

var name := "???"
var talk_speed := 5
var node: CanvasItem
var offset := Vector2()
var bleep: int = Bleep.Mid
var party: int = Party.INDEPENDENT
var opinion := ""


func _init(_name := ""):
	name = _name

func say(line: String, is_thinking := false) -> GDScriptFunctionState:
	var l := Line.new(line)
	l.speaker = self
	l.is_thinking = is_thinking
	return yield(Globals.dialogue_box.show_line(l), "completed")

func think(line: String) -> GDScriptFunctionState:
	return say(line, true)

# next: decorating methods follow Builder pattern

func attach(_node: CanvasItem) -> Speaker:
	assert(_node != null)
	node = _node
	if "speaker" in node:
		node.speaker = self
	if node is Sprite:
		# compute offset automatically
		offset = node.offset * 2
	return self

func offset_by(_offset: Vector2) -> Speaker:
	offset = _offset
	return self

func with_bleep(_bleep: int) -> Speaker:
	bleep = _bleep
	return self

func with_talk_speed(_speed: int) -> Speaker:
	talk_speed = _speed
	return self

func with_party(_party: int) -> Speaker:
	party = _party
	return self

func with_opinion(_op: String) -> Speaker:
	opinion = _op
	return self
