extends BaseButton


func _on_mouse_entered():
	if not disabled:
		print("yo")
		Globals.play_hover($Center.get_global_rect().position)


func _on_focus_entered():
	# If disabled, cancel focus (flicker focus mode)
	if disabled:
		set_deferred("focus_mode", focus_mode)
		focus_mode = Control.FOCUS_NONE
