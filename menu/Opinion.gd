extends Node2D


const party_names := {
	Speaker.Party.INDEPENDENT: "PARTY_IND",
	Speaker.Party.ANT: "PARTY_ANT",
	Speaker.Party.GRASSHOPPER: "PARTY_GRA",
	Speaker.Party.TERMITE: "PARTY_TER",
}
var chara: Character setget set_chara
onready var sprite: Sprite = $Sprite


func set_chara(new_chara: Character):
	chara = new_chara
	if not chara:
		return

	# Add sprite to icon
	sprite.texture = chara.texture
	sprite.offset = chara.offset
	sprite.region_enabled = chara.region_enabled
	sprite.region_rect = chara.region_rect
	sprite.hframes = chara.hframes
	sprite.vframes = chara.vframes

	assert(chara.speaker != null)
	var debater: Speaker = chara.speaker
	$Panel/Items/Name.text = debater.name
	$Panel/Items/Party.text = " " + tr(party_names[debater.party])
	$Sprite/Statement.text = debater.opinion
