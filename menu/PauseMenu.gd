extends Node2D

onready var test_sound: AudioStreamPlayer = $TestSound
onready var open_sound: AudioStreamPlayer = $Open
onready var backlog: Control = $"%Backlog"
var focused_object_before_pause: Control


func _ready():
	if "fr" in TranslationServer.get_locale():
		$"%fr".pressed = true
		$"%en".pressed = false
	# Pause on blur ONLY in web version (there is a music bug)
	Globals.pause_on_blur = OS.has_feature("web")


func _process(delta: float):
	# Move mouse cursor with gamepad
	var input := Input.get_vector("pointer_left", "pointer_right", "pointer_up", "pointer_down")
	if not input.is_zero_approx():
		get_viewport().warp_mouse(get_viewport().get_mouse_position() + 200 * input * delta)


func _input(event: InputEvent):
	# Click with gamepad A button
	if event.is_action("pointer_click"):
		var new_event := InputEventMouseButton.new()
		new_event.pressed = event.is_pressed()
		new_event.button_index = BUTTON_LEFT
		new_event.position = get_viewport().get_mouse_position()
		new_event.global_position = get_viewport().get_mouse_position()
		get_viewport().input(new_event)


func _on_Resume_pressed():
	hide()


func _on_PauseMenu_visibility_changed():
	get_tree().paused = visible
	if visible:
		focused_object_before_pause = $Bg.get_focus_owner()
		$PausePanel/Actions/Resume.grab_focus()
		open_sound.play()
	else:
		$Close.play()
		if focused_object_before_pause != null:
			focused_object_before_pause.grab_focus()


func _on_HiContrast_toggled(button_pressed: bool):
	test_sound.play()
	Globals.colorizer.visible = not button_pressed
	Globals.update_margins()


func pan_to(cell_x := 0):
	$Move.play()
	create_tween().tween_property(self, "position:x", float(-320 * cell_x), 0.5).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)
	match cell_x:
		-1:
			$BacklogPanel/Actions/Back.grab_focus()
		0:
			$PausePanel/Actions/Resume.grab_focus()
		1:
			$SettingsPanel/Actions/Back.grab_focus()


func _on_Settings_pressed():
	pan_to(1)


func _on_SettingsBack_pressed():
	pan_to(0)


func _on_MusicCheck_toggled(button_pressed: bool):
	AudioServer.set_bus_mute(1, not button_pressed)
	test_sound.play()
	$"%MusicSlider".value = 1 if button_pressed else 0


func _on_SfxCheck_toggled(button_pressed: bool):
	AudioServer.set_bus_mute(2, not button_pressed)
	test_sound.play()
	$"%SfxSlider".value = 1 if button_pressed else 0


func _on_MusicSlider_value_changed(value: float):
	AudioServer.set_bus_volume_db(1, linear2db(value))
	$"%MusicCheck".pressed = not is_zero_approx(value)


func _on_SfxSlider_value_changed(value: float):
	AudioServer.set_bus_volume_db(2, linear2db(value))
	$"%SfxCheck".pressed = not is_zero_approx(value)
	test_sound.play()


func _on_MainMenu_pressed():
	Globals.dialogue_box.abort_dialogue()
	hide()
	yield(Globals.transition(), "completed")
	var err := get_tree().change_scene_to(preload("res://menu/TitleScreen.tscn"))
	assert(err == OK)


func _on_Backlog_pressed():
	pan_to(-1)
	$BacklogPanel/Actions/Scroll.ensure_control_visible($"%BacklogBottom")


func _on_BacklogBack_pressed():
	pan_to(0)


func _on_en_toggled(pressed: bool):
	TranslationServer.set_locale("en" if pressed else "fr")
	if visible:
		test_sound.play()
	$"%fr".pressed = not pressed


func _on_fr_toggled(pressed: bool):
	TranslationServer.set_locale("fr" if pressed else "en")
	if visible:
		test_sound.play()
	$"%en".pressed = not pressed


func _on_CRTEffect_toggled(button_pressed: bool):
	test_sound.play()
	Globals.crt.visible = button_pressed
	Globals._root_viewport_size_changed()
