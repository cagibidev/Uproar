extends Node


onready var start_sound: AudioStreamPlayer = $Start


func _ready():
	if Globals.is_ending:
		Globals.is_ending = false
		$Title/Anim.stop()
		$HUD.hide()
		yield(Globals.dialogue_box.narrate(tr("TOTAL_SLASHED") + "\n" + str(Globals.nb_slashes)), "completed")
		yield(Globals.dialogue_box.narrate(tr("TOTAL_MISSED") + "\n" + str(Globals.nb_misses)), "completed")
		$HUD.show()
		$Title/Anim.play("enter")
		yield(get_tree().create_timer(1), "timeout")
		Globals.play_music("res://music/bar.mid")
	else:
		# TODO: regrab focus when unpaused
		$"%Play".grab_focus()


func _on_Play_pressed():
	Globals.set_ticket("normal")
	Globals.play_music("")
	start_sound.play()
	yield(Globals.transition(), "completed")
	get_tree().change_scene_to(preload("res://room/Plaza.tscn"))


func _on_MoreGames_pressed():
	OS.shell_open("https://cagibi.itch.io")


func _on_Support_pressed():
	OS.shell_open("https://www.buymeacoffee.com/cagibidev")


func _on_MusicRoom_pressed():
	start_sound.play()
	yield(Globals.transition(), "completed")
	get_tree().change_scene_to(preload("res://room/SoundRoom.tscn"))
