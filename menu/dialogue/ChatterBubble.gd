extends Node2D


func set_text(text: String):
	$Box/Message.text = text


func set_lifetime(lifetime := 2.0):
	$Timer.wait_time = lifetime


func _on_Timer_timeout():
	die()


func cancel_chatter():
	die()


func die():
	var tween := create_tween()
	tween.tween_property(self, "modulate:a", 0.0, 0.2)
	tween.tween_callback(self, "queue_free")
