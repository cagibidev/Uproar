extends Node2D
class_name DialogueBox

signal speaker_changed
signal chatter_changed
signal next_pressed

var current_speaker: Speaker setget set_speaker

var current_chatter := []
var chatter_index := 0

onready var msg_node: Label = $Anchor/Box/Message
onready var tail_node: Node = $Anchor/Tail
onready var char_timer: Timer = $CharTimer
onready var talk_sound: Node = $Bleeps/Low
onready var next_sound: AudioStreamPlayer = $Next
onready var arrow: Node2D = $Anchor/Box/Control/NextArrow
onready var name_container: CanvasItem = $Anchor/Box/Name
onready var name_node: Label = $Anchor/Box/Name/Backdrop/Label


func set_speaker(new: Speaker):
	current_speaker = new
	emit_signal("speaker_changed", new)


func _ready():
	create_tween().set_loops().tween_callback(self, "bop_arrow").set_delay(0.5)


func bop_arrow():
	arrow.position.x = 6 if arrow.position.x > 8 else 10


func show_line(l: Line) -> GDScriptFunctionState:
	next_sound.play()
	_next_line(l)
	return yield(self, "next_pressed")


func _next_line(line: Line = null):
	show()

	msg_node.text = line.message
	msg_node.percent_visible = 0

	arrow.hide()
	name_container.hide()
	tail_node.show()

	if line.is_thinking:
		tail_node.hide()

	var target_bubble_position := Vector2(160, 0)
	msg_node.rect_min_size.x = 224

	if current_speaker != line.speaker:
		set_speaker(line.speaker)
		Globals.add_speaker_to_backlog(line.speaker)
	Globals.add_to_backlog(line)

	var box: StyleBox = preload("res://menu/dialogue/speech.stylebox")
	msg_node.modulate = Color.black
	tail_node.modulate = Color.white

	if line.speaker:
		if current_speaker.name:
			name_container.show()
			name_node.text = current_speaker.name
		set_bleep(current_speaker.bleep)
		if line.is_thinking:
			box = preload("res://menu/dialogue/think.stylebox")
		if line.speaker.name == "N_FORMINISTER":
			msg_node.modulate = Color.white
			tail_node.modulate = Color("#a000")
			box = preload("res://menu/dialogue/forminister.stylebox")

		if current_speaker.name == "N_PRES": # Chairman always speaks at screen bottom
			target_bubble_position.y = 180
			tail_node.hide()
		elif current_speaker.node:
			target_bubble_position = current_speaker.node.global_position + line.speaker.offset
			tail_node.position.x = 0
		else:
			tail_node.hide()
	else:
		set_bleep(Speaker.Bleep.Type)
		tail_node.hide()
		box = preload("res://menu/dialogue/prompt.stylebox")
	$Anchor/Box.add_stylebox_override("panel", box)

	# Adjust bubble horizontally
	var min_w := 40
	msg_node.rect_min_size.x = min_w
	msg_node.rect_size.x = min_w

	# Give text time to settle in bubble
	yield(get_tree(), "idle_frame")
	yield(get_tree(), "idle_frame")

	var margin_sides := 8.0
	var width: float = $Anchor/Box.rect_size.x
	var min_x := (width/2)+margin_sides
	if target_bubble_position.x < min_x:
		tail_node.position.x = target_bubble_position.x - min_x
		target_bubble_position.x = min_x
	var max_x := 320 - min_x
	if target_bubble_position.x > max_x:
		tail_node.position.x = target_bubble_position.x - max_x
		target_bubble_position.x = max_x

	# Adjust bubble vertically
	var margin_top := 14 if (current_speaker and current_speaker.name) else 8
	var margin_bottom := 0
	if target_bubble_position.y + $Anchor/Box.rect_position.y < margin_top:
		tail_node.hide()
		target_bubble_position.y = margin_top - $Anchor/Box.rect_position.y
	if target_bubble_position.y > 180 - margin_bottom:
		tail_node.hide()
		target_bubble_position.y = 180 - margin_bottom

	create_tween() \
		.tween_property($Anchor, "global_position", target_bubble_position, 0.2) \
		.set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)

	char_timer.start(0.3)


func set_bleep(b: int):
	match b:
		Speaker.Bleep.High: talk_sound = $Bleeps/High
		Speaker.Bleep.Mid: talk_sound = $Bleeps/Mid
		Speaker.Bleep.Low: talk_sound = $Bleeps/Low
		Speaker.Bleep.VeryLow: talk_sound = $Bleeps/VeryLow
		Speaker.Bleep.Type: talk_sound = $Bleeps/Type


func _on_gui_input(event: InputEvent):
	if event is InputEventMouseButton and event.button_index == BUTTON_LEFT and not event.is_pressed():
		_on_click()


func _unhandled_input(event: InputEvent):
	if visible and event.is_action_released("ui_accept"):
		_on_click()


func _on_click():
	if msg_node.percent_visible >= 1:
		hide()
		emit_signal("next_pressed")
		if not visible:
			set_speaker(null)
	else:
		msg_node.percent_visible = 1
		char_timer.stop()
		arrow.show()


func abort_dialogue():
	hide()
	set_speaker(null)


func _on_CharTimer_timeout():
	var time := 0.1
	var nb_chars := current_speaker.talk_speed if current_speaker else 5
	var should_play_sound := true

	# reveal characters progressively
	for i in range(nb_chars):
		if msg_node.percent_visible >= 1:
			break
		# get the last char
		var visible_text := tr(msg_node.text).replace(" ", "").replace("\n", "")
		var new_char := visible_text[msg_node.visible_characters]
		msg_node.visible_characters += 1
		# on punctuation, wait a bit longer
		if new_char in ".,;:?!":
			time = 0.25
			if i == 0:
				should_play_sound = false
			break

	char_timer.start(time)

	# handle end of message
	if msg_node.percent_visible >= 1:
		char_timer.stop()
		arrow.show()

	# play talk sound
	if should_play_sound:
		if talk_sound is AudioStreamPlayer:
			talk_sound.play()
		else:
			talk_sound.get_child(randi() % talk_sound.get_child_count()).play()


func show_chatter_bubble():
	var bubble: Node2D = preload("res://menu/dialogue/ChatterBubble.tscn").instance()
	get_parent().add_child(bubble)
	var speaker: Speaker = current_chatter[chatter_index][0]
	assert(speaker.node != null)
	bubble.global_position = speaker.node.global_position + speaker.offset
	bubble.set_text(current_chatter[chatter_index][1])
	if len(current_chatter) > 1:
		$CommentTimer.start()

	# When chatter ends, bubble dies instantly
	self.connect("chatter_changed", bubble, "cancel_chatter")


func _on_CommentTimer_timeout():
	# Show next chatter bubble
	chatter_index += 1
	if chatter_index >= len(current_chatter):
		chatter_index = 0
	show_chatter_bubble()


func set_chatter(messages := []):
	emit_signal("chatter_changed")
	$CommentTimer.stop()
	current_chatter = messages
	if len(messages) > 0:
		chatter_index = 0
		show_chatter_bubble()


func narrate(line: String) -> GDScriptFunctionState:
	return show_line(Line.new(line))
