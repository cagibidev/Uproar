extends BaseButton

signal selected

export (bool) var has_opinion := false
var has_checked := false setget set_checked


func set_checked(new_checked: bool):
	has_checked = new_checked
	$Center/Check.visible = has_checked
	$Circle.modulate.a = 0.333 if has_checked else 1.0


func set_scanned(scanned: bool):
	yield(get_tree().create_timer(rect_global_position.x / 800.0), "timeout")
	if scanned:
		$Center/Scanned.play()
		$Anim.play("appear")
	else:
		$Anim.play("disappear")


func _ready():
	$Circle/Opi.visible = has_opinion
	$Circle.rect_pivot_offset = rect_size / 2
	$Circle/Opi.rect_pivot_offset = rect_size / 2


func set_enabled(enabled: bool):
	disabled = not enabled
	mouse_filter = Control.MOUSE_FILTER_IGNORE if disabled else Control.MOUSE_FILTER_PASS
	if not enabled and $Anim.current_animation == "glow":
		_on_mouse_exited()


func _on_mouse_entered():
	$Center/Hovered.play()
	$Anim.play("appear")


func _on_mouse_exited():
	$Anim.play("disappear")


func _on_pressed():
	emit_signal("selected")


func _on_Anim_animation_finished(anim_name: String):
	if anim_name == "appear":
		$Anim.play("glow")
	if anim_name == "disappear":
		$Anim.play("invisible")
