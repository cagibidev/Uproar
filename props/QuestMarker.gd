extends Sprite


func _on_visibility_changed():
	if visible:
		$Anim.play("appear")
		yield($Anim, "animation_finished")
		$Anim.play("noticeme")
