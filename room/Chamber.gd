extends "res://room/Room.gd"


var selected_characters := [null, null]
var invectives := 0 setget set_invectives
export (String, FILE, "*.mid") var music := "res://music/debate_low.mid"

onready var scara := Speaker.new("N_SCARA") \
	.attach($Near/Scara/C)
onready var cass := Speaker.new("N_CASS").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Cass/C)
onready var president := Speaker.new("N_PRES").attach($Nearest/Pres/C)
onready var carp := Speaker.new("N_CARP").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Carp/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_CARP")
onready var jp := Speaker.new("N_JP") \
	.attach($Near/JP/C) \
	.with_party(Speaker.Party.GRASSHOPPER).with_opinion("2_OPI_JP")
onready var cicada := Speaker.new("N_CICADA") \
	.attach($Near/Singcada/C) \
	.with_party(Speaker.Party.GRASSHOPPER).with_opinion("2_OPI_CICADA")
onready var locust := Speaker.new("N_LOCUST") \
	.attach($Near/Locust/C) \
	.with_party(Speaker.Party.GRASSHOPPER).with_opinion("2_OPI_LOCUST")
onready var warrior := Speaker.new("N_WARRIOR").with_bleep(Speaker.Bleep.Low) \
	.attach($Near/Warrior/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_WARRIOR")
onready var fisher := Speaker.new("N_FISHER") \
	.attach($Near/Fisher/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_FISHER")
onready var explorer := Speaker.new("N_EXPLORER") \
	.attach($Near/Explorer/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_EXPLORER")
onready var farmer := Speaker.new("N_FARMER").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Farmer/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_FARMER")
onready var weaver := Speaker.new("N_WEAVER").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Weaver/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_WEAVER")
onready var nurse := Speaker.new("N_NURSE").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Nurse/C) \
	.with_party(Speaker.Party.ANT).with_opinion("2_OPI_NURSE")
onready var allG := Speaker.new("N_ALLG")


func set_invectives(new_inv: int):
	invectives = new_inv
	$HUD/Actions/Provoke.disabled = invectives <= 0


func _ready():
	Globals.connect("heat_changed", self, "_on_heat_changed")

	for chara in get_tree().get_nodes_in_group("character"):
		setup_mp(chara)
	intro_spiel()


func setup_mp(chara: Character):
	var poi: Node = chara.get_node_or_null("../POI")
	if poi == null or chara.speaker == null or not chara.speaker.opinion:
		return
	poi.connect("selected", self, "character_selected", [chara])


func intro_spiel():
	$Near/JP/Anim.play("absent")
	set_invectives(0)
	set_cutscene(true)
	$Camera.position.y = 32
	create_tween().tween_property($Camera, "position:y", 0, 2).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)
	yield(get_tree().create_timer(1), "timeout")
	yield(Globals.dialogue_box.narrate("2_INTRO_1"), "completed")
	Globals.play_music("res://music/story.mid")
	yield(president.say("2_INTRO_2_PRES"), "completed")
	yield(president.say("2_INTRO_3_PRES"), "completed")
	yield(president.say("2_INTRO_4_PRES"), "completed")
	yield(carp.say("2_INTRO_5_CARP"), "completed")
	yield(carp.say("2_INTRO_6_CARP"), "completed")
	yield(carp.say("2_INTRO_7_CARP"), "completed")
	yield(carp.say("2_INTRO_8_CARP"), "completed")
	yield(cicada.say("2_INTRO_9_GRA"), "completed")
	yield(carp.say("2_INTRO_10_CARP"), "completed")
	flip(scara, Character.Mood.SHOCK, false)
	yield(scara.say("2_INTRO_11_SCARA"), "completed")
	yield(carp.say("2_INTRO_12_CARP"), "completed")
	Globals.play_music("")
	flip(scara, Character.Mood.NEUTRAL, false)
	flip(cass, Character.Mood.ANGRY, false)
	yield(cass.say("2_INTRO_13_CASS"), "completed")
	yield(locust.say("2_INTRO_14_GRA"), "completed")
	# TODO: correct progressive heat
	Globals.heat += 10
	yield(get_tree().create_timer(1.5), "timeout")
	$Uproar.play()

	Globals.dialogue_box.set_chatter([
		[carp, "2_INTRO_15_CHAT1"],
		[weaver, "2_INTRO_15_CHAT2"],
		[explorer, "2_INTRO_15_CHAT3"],
		[farmer, "2_INTRO_15_CHAT4"]
	])
	yield(Globals.dialogue_box.narrate("2_INTRO_15"), "completed")
	Globals.dialogue_box.set_chatter()
	yield(fisher.say("2_INTRO_16_ANT"), "completed")
	yield(nurse.say("2_INTRO_17_ANT"), "completed")
	yield(warrior.say("2_INTRO_18_WARRIOR"), "completed")
	$Near/JP/Anim.play("slide")
	yield($Near/JP/Anim, "animation_finished")
	flip(cass, Character.Mood.THINK, true)

	Globals.dialogue_box.set_chatter([
		[president, "2_JPSAVE_2_CHAT1"],
		[cass, "2_JPSAVE_2_CHAT2"],
		[scara, "2_JPSAVE_2_CHAT3"],
		[locust, "2_JPSAVE_2_CHAT4"],
		[warrior, "2_JPSAVE_2_CHAT5"]
	])
	yield(jp.say("2_JPSAVE_1_JP"), "completed")
	flip(jp, Character.Mood.NEUTRAL, false)
	yield(jp.say("2_JPSAVE_4_JP"), "completed")
	Globals.play_music("res://music/story.mid")
	yield(jp.say("2_JPSAVE_5_JP"), "completed")
	yield(jp.say("2_JPSAVE_6_JP"), "completed")
	flip(jp, Character.Mood.ANGRY, false)
	yield(jp.say("2_JPSAVE_7_JP"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	flip(scara, Character.Mood.NEUTRAL, false)
	$Uproar.play()

	Globals.dialogue_box.set_chatter([
		[fisher, "2_JPSAVE_8_CHAT1"],
		[weaver, "2_JPSAVE_8_CHAT2"],
		[scara, "2_JPSAVE_8_CHAT3"],
	])
	yield(carp.say("2_JPSAVE_8_ANT"), "completed")
	yield(explorer.say("2_JPSAVE_9_ANT"), "completed")
	$Uproar.stop()
	Globals.dialogue_box.set_chatter()
	yield(jp.say("2_JPSAVE_10_JP"), "completed")
	Globals.dialogue_box.set_chatter([
		[cass, "2_JPSAVE_11_CHAT_CASS1"],
		[cass, "2_JPSAVE_11_CHAT_CASS2"],
		[cass, "2_JPSAVE_11_CHAT_CASS3"],
		[cass, "2_JPSAVE_11_CHAT_CASS4"],
	])
	yield(farmer.say("2_JPSAVE_11_ANT"), "completed")
	yield(farmer.say("2_JPSAVE_11_2_ANT"), "completed")
	Globals.dialogue_box.set_chatter()
	yield(scara.think("2_JPSAVE_12_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, true)
	flip(jp, Character.Mood.NEUTRAL, false)
	yield(scara.say("2_JPSAVE_13_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	Globals.play_music("")
	var debate_start = preload("res://effects/milestones/DebateStart.tscn").instance()
	add_child(debate_start)
	yield(debate_start, "tree_exited")

	Globals.play_music(music)
	set_cutscene(false)
	set_invectives(1)


func jp_land():
	$Uproar.stop()
	$Near/JP/Land.play()
	flip(cass, Character.Mood.SHOCK, true)
	flip(scara, Character.Mood.NEUTRAL, true)
	set_shake(true)
	yield(get_tree().create_timer(0.2), "timeout")
	set_shake(false)


func toggle_chara_opinion(chara: Character, is_active: bool):
	if is_active:
		if chara == jp.node:
			flip(jp, Character.Mood.ANGRY, false)
			chara.anim_node.play("jump")
		else:
			chara.anim_node.play("fast")
	else:
		if chara == jp.node and Globals.heat < 50:
			flip(jp, Character.Mood.NEUTRAL, false)
		chara.anim_node.play("normal")


func character_selected(chara: Character):
	var compare_anim: AnimationPlayer = $HUD/Actions/Compare/Anim

	toggle_chara_opinion(chara, true)
	if chara in selected_characters:
		# Deselect character instead
		toggle_chara_opinion(chara, false)
		if chara == selected_characters[0]:
			if selected_characters[1]:
				# second becomes first
				selected_characters[0] = selected_characters[1]
				selected_characters[1] = null
				$HUD/Op1/Op2.chara = null
				$HUD/Op1.chara = selected_characters[0]
				$HUD/Op1/Anim.play("second_disappear")
				$HUD/Actions/Compare.hide()
				compare_anim.stop()
				return
			# from 1 to 0 character
			selected_characters[0] = null
			$HUD/Op1.chara = null
			$HUD/Op1/Anim.play("disappear")
			
			$HUD/Poll.show()
			$HUD/Hint.show()
			$HUD/Actions/Provoke.show()
			$HUD/Actions/Back.hide()
		elif chara == selected_characters[1]:
			# from 2 to 1 character, in order
			selected_characters[1] = null
			$HUD/Op1/Op2.chara = null
			$HUD/Op1/Anim.play("second_disappear")

			$HUD/Actions/Compare.hide()
			compare_anim.stop()
		return

	if selected_characters[0]:
		# add second character
		if selected_characters[1]:
			# do a switcharoo if there's already 2 charas
			toggle_chara_opinion(selected_characters[1], false)
		selected_characters[1] = chara
		$HUD/Op1/Op2.chara = chara
		$HUD/Actions/Compare.show()
		compare_anim.play("noticeme")
		$HUD/Op1/Anim.play("second_appear")
	else:
		# from 0 to 1 character
		selected_characters[0] = chara
		$HUD/Op1.chara = chara
		$HUD/Op1/Anim.play("appear")
		$HUD/Poll.hide()
		$HUD/Hint.hide()
		$HUD/Actions/Provoke.hide()
		$HUD/Actions/Back.show()


func _on_heat_changed(old_heat: float, new_heat: float):
	$Nearest/Heat/Anim.play("up" if new_heat > old_heat else "down")
	create_tween().tween_property($Nearest/Heat, "value", new_heat, 1)


func _on_Back_pressed():
	if selected_characters[1]:
		character_selected(selected_characters[1])
	elif selected_characters[0]:
		character_selected(selected_characters[0])


func _on_Compare_pressed():
	set_cutscene(true)

	if both_are_selected(explorer, fisher):
		yield(enter_minigame(preload("res://bullets/SearchBoard.tscn"), [explorer, fisher]), "completed")
		$Near/Explorer/POI.set_checked(true)
		$Near/Fisher/POI.set_checked(true)
	elif both_are_selected(weaver, carp):
		yield(enter_minigame(preload("res://bullets/FlyBoard.tscn"), [weaver, carp]), "completed")
		$Near/Weaver/POI.set_checked(true)
		$Near/Carp/POI.set_checked(true)
	elif both_are_selected(farmer, jp):
		yield(enter_minigame(preload("res://bullets/TimeBoard.tscn"), [farmer, jp]), "completed")
		$Near/JP/POI.set_checked(true)
		$Near/Farmer/POI.set_checked(true)
	else:
		flip(scara, Character.Mood.ANGRY, false)
		if selected_characters[0].speaker.party == Speaker.Party.GRASSHOPPER or selected_characters[1].speaker.party == Speaker.Party.GRASSHOPPER:
			# Grasshoppers can't contradict
			yield(interject(false), "completed")
			flip(scara, Character.Mood.THINK, false)
			yield(scara.say("2_GRA_1_SCARA"), "completed")
			yield(cass.say("2_GRA_2_CASS"), "completed")
			yield(scara.say("2_GRA_3_SCARA"), "completed")
		else:
			# Not a contradiction
			yield(interject(false), "completed")
			flip(scara, Character.Mood.ANGRY, false)
			yield(scara.say("2_FAIL_1"), "completed")
			flip(scara, Character.Mood.THINK, false)
			yield(cass.say("2_FAIL_2"), "completed")
			yield(president.say("2_FAIL_3"), "completed")
			yield(president.say("2_FAIL_4"), "completed")
			yield(Globals.dialogue_box.narrate("2_FAIL_5"), "completed")

	set_cutscene(false)
	check_ending_condition()


func interject(is_success: bool) -> GDScriptFunctionState:
	# Close any selected opinions (silently)
	$HUD/Op1/Cancel.volume_db = -80
	_on_Back_pressed()
	_on_Back_pressed()
	
	flip(scara, Character.Mood.ANGRY, false)
	if is_success:
		Globals.play_music("")
	var interjection := preload("res://effects/milestones/Interjection.tscn").instance()
	interjection.get_node("Blast/Tapped").volume_db = -80
	add_child(interjection)
	yield($HUD/Op1/Cancel, "finished")
	$HUD/Op1/Cancel.volume_db = 0
	return yield(interjection, "tree_exited")


# shorthand that returns if both MPs are selected
func both_are_selected(speaker1: Speaker, speaker2: Speaker) -> bool:
	if "MIND_CHANGED" in speaker1.opinion:
		return false
	return (selected_characters[0].speaker == speaker1 \
		and selected_characters[1].speaker == speaker2) or ( \
		selected_characters[1].speaker == speaker1 \
		and selected_characters[0].speaker == speaker2)


func enter_minigame(scene: PackedScene, arguers: Array) -> GDScriptFunctionState:
	yield(interject(true), "completed")

	var board := scene.instance()
	add_child(board)
	var res: GDScriptFunctionState = yield(board, "completed")
	board.queue_free()
	solve_contradiction(arguers)
	return res


func solve_contradiction(arguers: Array):
	for arguer in arguers:
		assert(arguer is Speaker)
		arguer.opinion = "MIND_CHANGED"
		if arguer.name == "N_WARRIOR":
			arguer.opinion = "MIND_CHANGED_WARRIOR"
	Globals.play_music(music)
	Globals.heat += 10
	flip(scara, Character.Mood.THINK, false)
	update_votes()


func update_votes():
	# Update vote results
	var yes: int = 8 - int(Globals.heat / 10.0)
	var no: int = int(Globals.heat / 10.0)
	var res := ""
	for _i in range(yes):
		res += "*"
	res += "\n"
	for _i in range(no):
		res += "*"
	res += "\n***"
	$HUD/Poll/Results.text = res


func check_ending_condition():
	if Globals.heat > 35:
		end_session()


func end_session():
	# End of part 1: Lunch break
	set_cutscene(true)
	Globals.play_music("")
	$Uproar.play()
	flip(scara, Character.Mood.HAPPY, false)

	yield(president.say("2_END_0_PRES"), "completed")
	yield(president.say("2_END_1_PRES"), "completed")
	yield(president.say("2_END_2_PRES"), "completed")
	yield(scara.think("2_END_3_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(president.say("2_END_4_PRES"), "completed")
	yield(president.say("2_END_5_PRES"), "completed")
	yield(carp.say("2_END_6_CARP"), "completed")
	$Uproar.stop()
	yield(warrior.say("2_END_7_WARRIOR"), "completed")

	$Nearest/PresBench/CoffeeAnim.play("ring")
	$Nearest/PresBench/Coffee/Ring.play()
	yield(get_tree().create_timer(1.5), "timeout")
	$Nearest/PresBench/Coffee/Ring.stop()

	Globals.dialogue_box.set_chatter([
		[warrior, "2_END_CHATTER_1"],
		[cass, "2_END_CHATTER_2"],
		[cicada, "2_END_CHATTER_3"],
		[weaver, "2_END_CHATTER_4"],
		[locust, "2_END_CHATTER_5"],
	])
	flip(cass, Character.Mood.THINK, false)
	yield(president.say("2_END_8_PRES"), "completed")
	$Uproar.play()
	Globals.heat = 10
	yield(president.say("2_END_9_PRES"), "completed")
	yield(scara.say("2_END_10_SCARA"), "completed")
	Globals.dialogue_box.set_chatter()
	yield(Globals.transition(), "completed")
	get_tree().change_scene_to(preload("res://room/stairs/Stairs.tscn"))


func _on_Provoke_pressed():
	$HUD/Actions/Provoke/Sound.play()
	set_invectives(invectives - 1)
	set_shake(true)
	yield(get_tree().create_timer(0.1), "timeout")
	set_shake(false)

	set_cutscene(true)
	var clash: String = [
		"CLASH_1_1",
		"CLASH_1_2",
		"CLASH_1_3",
	][randi()%3]

	flip(scara, Character.Mood.ANGRY, false)
	flip(cass, Character.Mood.THINK, true)

	yield(scara.say(clash), "completed")
	set_shake(true)
	$Uproar.play()
	yield(allG.say("CLASH_2_ALLG"), "completed")
	$Uproar.stop()
	set_shake(false)
	Globals.heat += 10
	yield(get_tree().create_timer(1), "timeout")
	flip(scara, Character.Mood.NEUTRAL, false)
	flip(cass, Character.Mood.THINK, false)
	yield(president.say("CLASH_3_PRES"), "completed")
	yield(scara.say("CLASH_4_SCARA"), "completed")
	yield(Globals.dialogue_box.narrate("CLASH_5"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	set_cutscene(false)
	update_votes()
	check_ending_condition()


func _on_Thermometer_animation_finished(_anim_name):
	pass


func _on_President_selected():
	yield(tap($Nearest/Pres), "completed")
	
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.think("2_PRES_1_SCARA"), "completed")
	yield(scara.say("2_PRES_2_SCARA"), "completed")
	flip(scara, Character.Mood.THINK, false)
	yield(president.say("2_PRES_3_PRES"), "completed")
	yield(cass.say("2_PRES_4_CASS"), "completed")
	yield(president.say("2_PRES_5_PRES"), "completed")
	yield(president.say("2_PRES_6_PRES"), "completed")
	yield(cass.say("2_PRES_7_CASS"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	$Nearest/POI.set_checked(true)
	set_cutscene(false)


func _on_ScaraCass_selected():
	yield(tap($Near/Cass/POI), "completed")
	
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(scara.say("2_CASS_1_SCARA"), "completed")
	yield(cass.say("2_CASS_2_CASS"), "completed")
	flip(scara, Character.Mood.HAPPY, false)
	yield(scara.say("2_CASS_3_SCARA"), "completed")
	yield(scara.say("2_CASS_4_SCARA"), "completed")
	yield(scara.say("2_CASS_5_SCARA"), "completed")
	yield(cass.say("2_CASS_6_CASS"), "completed")
	yield(scara.say("2_CASS_7_SCARA"), "completed")
	flip(cass, Character.Mood.THINK, true)
	yield(cass.say("2_CASS_8_CASS"), "completed")
	flip(scara, Character.Mood.THINK, false)
	yield(scara.say("2_CASS_9_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	yield(cass.say("2_CASS_10_CASS"), "completed")
	yield(scara.think("2_CASS_11_SCARA"), "completed")
	$Near/Cass/POI.set_checked(true)
	set_cutscene(false)


func emit_compare_aura():
	var b: Node2D = blast.instance()
	add_child(b)
	b.global_position = $HUD/Actions/Compare/Auras.global_position
	b.scale = Vector2(0.5, 0.25)
	b.get_node("Anim").playback_speed = 0.5
	b.get_node("Tapped").volume_db = -80
	b.z_index = 0
