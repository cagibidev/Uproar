extends "res://room/Chamber.gd"


onready var cricket := Speaker.new("N_CRICKET") \
	.attach($Near/Cricket/C) \
	.with_party(Speaker.Party.GRASSHOPPER).with_opinion("4_OPI_CRICKET")
onready var thermite := Speaker.new("N_THERMITE") \
	.attach($Near/Thermite/C) \
	.with_party(Speaker.Party.GRASSHOPPER).with_opinion("4_OPI_THERMITE")
onready var weta := Speaker.new("N_WETA").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Weta/C) \
	.with_party(Speaker.Party.GRASSHOPPER).with_opinion("4_OPI_WETA")
onready var warrior_off := Speaker.new("N_WARRIOR").with_bleep(Speaker.Bleep.Low)


func _ready():
	# Link missing characters
	for chara in [weta.node, thermite.node, cricket.node]:
		setup_mp(chara)

	# solve all grasshoppers
	for gra in [weta, thermite, cricket, cicada, locust, jp]:
		gra.node.get_node("../POI").set_checked(true)

	# Update opinions
	cicada = cicada.with_opinion("4_OPI_CICADA")
	locust = locust.with_opinion("4_OPI_LOCUST")
	jp = jp.with_opinion("4_OPI_JP")
	weaver = weaver.with_opinion("4_OPI_WEAVER")
	explorer = explorer.with_opinion("4_OPI_EXPLORER")
	nurse = nurse.with_opinion("4_OPI_NURSE")
	farmer = farmer.with_opinion("4_OPI_FARMER")
	carp = carp.with_opinion("4_OPI_CARP")
	fisher = fisher.with_opinion("4_OPI_FISHER")
	warrior = warrior.with_opinion("4_OPI_WARRIOR")


func intro_spiel():
	set_invectives(1)
	set_cutscene(true)
	$Camera.position.y = 32
	create_tween().tween_property($Camera, "position:y", 0.0,2).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)
	yield(get_tree().create_timer(1), "timeout")

	yield(Globals.dialogue_box.narrate("4_INTRO_0"), "completed")
	Globals.play_music("res://music/story.mid")
	yield(president.say("4_INTRO_1_PRES"), "completed")
	Globals.dialogue_box.set_chatter([
		[cricket, "4_INTRO_CHAT_GRA_1"],
		[thermite, "4_INTRO_CHAT_GRA_2"],
		[weta, "4_INTRO_CHAT_GRA_3"],
	])
	yield(president.say("4_INTRO_2_PRES"), "completed")
	yield(president.say("4_INTRO_3_PRES"), "completed")
	Globals.dialogue_box.set_chatter()
	yield(president.say("4_INTRO_4_PRES"), "completed")
	flip(scara, Character.Mood.ANGRY, false)
	Globals.play_music("")
	yield(scara.say("4_INTRO_5_SCARA"), "completed")
	yield(scara.say("4_INTRO_6_SCARA"), "completed")
	$Near/Warrior/Anim.play("break")
	yield($Near/Warrior/Anim, "animation_finished")

	yield(warrior.say("4_INTRO_7_WARRIOR"), "completed")
	yield(warrior.say("4_INTRO_8_WARRIOR"), "completed")
	yield(warrior.say("4_INTRO_9_WARRIOR"), "completed")
	yield(warrior.say("4_INTRO_10_WARRIOR"), "completed")
	Globals.play_music("res://music/truth.mid")
	yield(warrior.say("4_INTRO_11_WARRIOR"), "completed")
	flip(jp, Character.Mood.ANGRY, false)
	$Uproar.play()
	Globals.dialogue_box.set_chatter([
		[cass, "4_CHAT_1_CASS"],
		[fisher, "4_CHAT_2_ANT"],
		[explorer, "4_CHAT_3_ANT"],
		[jp, "4_CHAT_4_JP"],
		[president, "4_CHAT_5_PRES"],
	])
	yield(scara.say("4_INTRO_12_SCARA"), "completed")
	yield(warrior.say("4_INTRO_13_WARRIOR"), "completed")
	flip(warrior, Character.Mood.NEUTRAL, false)
	yield(warrior.say("4_INTRO_14_WARRIOR"), "completed")
	Globals.dialogue_box.set_chatter()
	$FlashBernard/Anim.play("flash")
	yield(warrior_off.say("4_INTRO_15_WARRIOR"), "completed")
	yield(warrior_off.say("4_INTRO_16_WARRIOR"), "completed")
	$FlashBernard/Anim.play("end")
	yield(warrior.say("4_INTRO_17_WARRIOR"), "completed")
	flip(scara, Character.Mood.ANGRY, false)
	yield(scara.say("4_INTRO_18_SCARA"), "completed")
	yield(scara.say("4_INTRO_19_SCARA"), "completed")
	Globals.dialogue_box.set_chatter([
		[weaver, "4_CHAT_6"],
		[carp, "4_CHAT_7"],
		[farmer, "4_CHAT_8"],
	])
	flip(cass, Character.Mood.ANGRY, true)
	yield(cass.say("4_INTRO_20_CASS"), "completed")
	Globals.dialogue_box.set_chatter()
	flip(warrior, Character.Mood.HAPPY, true)
	flip(cass, Character.Mood.ANGRY, false)
	yield(warrior.say("4_INTRO_21_WARRIOR"), "completed")
	flip(warrior, Character.Mood.LOOKAWAY, true)
	yield(warrior.say("4_INTRO_22_WARRIOR"), "completed")
	flip(scara, Character.Mood.SAD, false)
	yield(scara.say("4_INTRO_23_SCARA"), "completed")
	yield(carp.say("4_INTRO_24_ANT"), "completed")
	yield(nurse.say("4_INTRO_25_ANT"), "completed")
	yield(fisher.say("4_INTRO_26_ANT"), "completed")
	yield(weaver.say("4_INTRO_27_ANT"), "completed")
	yield(warrior.say("4_INTRO_28_WARRIOR"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(jp.say("4_INTRO_29_JP"), "completed")
	yield(jp.say("4_INTRO_30_JP"), "completed")
	Globals.play_music("")
	yield(scara.say("4_INTRO_31_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.think("4_INTRO_32_SCARA"), "completed")
	yield(scara.think("4_INTRO_33_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	$Uproar.stop()

	var debate_start = preload("res://effects/milestones/DebatePart2.tscn").instance()
	add_child(debate_start)
	yield(debate_start, "tree_exited")

	$Uproar.play()
	Globals.play_music(music)
	set_cutscene(false)


func warrior_strike():
	set_shake(true)
	flip(scara, Character.Mood.SHOCK, false)
	flip(cass, Character.Mood.SHOCK, false)
	$Uproar.stop()
	Globals.heat = 60
	$Nearest/HeatEffect.show()
	for db in [0, -10, -20]:
		$Near/Warrior/Smash.volume_db = db
		$Near/Warrior/Smash.play()
		yield(get_tree().create_timer(0.7), "timeout")
		set_shake(false)


func _on_Thermometer_animation_finished(_anim_name):
	$Nearest/Heat/Anim.play("phase_2")


func _on_President_selected():
	flip(scara, Character.Mood.THINK, false)
	yield(tap($Nearest/Pres), "completed")
	yield(scara.say("4_PRES_1_SCARA"), "completed")
	flip(cass, Character.Mood.THINK, true)
	yield(president.say("4_PRES_2_PRES"), "completed")
	yield(president.say("4_PRES_3_PRES"), "completed")
	yield(cass.say("4_PRES_4_CASS"), "completed")
	yield(president.say("4_PRES_5_PRES"), "completed")
	flip(cass, Character.Mood.THINK, false)
	yield(scara.think("4_PRES_6_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	flip(cass, Character.Mood.NEUTRAL, false)
	$Nearest/POI.set_checked(true)
	set_cutscene(false)


func _on_ScaraCass_selected():
	flip(scara, Character.Mood.THINK, false)
	yield(tap($Near/Cass/POI), "completed")
	flip(cass, Character.Mood.ANGRY, true)
	yield(cass.say("4_CASS_1_CASS"), "completed")
	yield(scara.say("4_CASS_2_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	yield(scara.say("4_CASS_3_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	flip(scara, Character.Mood.SAD, false)
	yield(cass.say("4_CASS_4_CASS"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.say("4_CASS_5_SCARA"), "completed")
	$Near/Cass/POI.set_checked(true)
	set_cutscene(false)


func _on_Compare_pressed():
	set_cutscene(true)

	if both_are_selected(nurse, warrior):
		yield(enter_minigame(preload("res://bullets/CutBoard.tscn"), [nurse, warrior]), "completed")
		$Near/Nurse/POI.set_checked(true)
		$Near/Warrior/POI.set_checked(true)
	elif both_are_selected(fisher, carp):
		yield(enter_minigame(preload("res://bullets/AssaultBoard.tscn"), [fisher, carp]), "completed")
		$Near/Carp/POI.set_checked(true)
		$Near/Fisher/POI.set_checked(true)
	elif both_are_selected(farmer, explorer):
		yield(enter_minigame(preload("res://bullets/GunBoard.tscn"), [farmer, explorer]), "completed")
		$Near/Explorer/POI.set_checked(true)
		$Near/Farmer/POI.set_checked(true)
	else:
		if selected_characters[0].speaker.party == Speaker.Party.GRASSHOPPER or selected_characters[1].speaker.party == Speaker.Party.GRASSHOPPER:
			# Grasshoppers can't contradict
			yield(interject(false), "completed")
			flip(scara, Character.Mood.THINK, false)
			yield(scara.say("4_GRA_1_SCARA"), "completed")
			yield(cass.say("4_GRA_2_CASS"), "completed")
			yield(scara.say("4_GRA_3_SCARA"), "completed")
		else:
			# Not a contradiction
			yield(interject(false), "completed")
			flip(scara, Character.Mood.ANGRY, false)
			yield(scara.say("2_FAIL_1"), "completed")
			flip(scara, Character.Mood.THINK, false)
			yield(cass.say("2_FAIL_2"), "completed")
			yield(president.say("2_FAIL_3"), "completed")
			yield(president.say("2_FAIL_4"), "completed")
			yield(Globals.dialogue_box.narrate("2_FAIL_5"), "completed")

	set_cutscene(false)
	$Uproar.play()
	check_ending_condition()


func update_votes():
	# No votes to update
	$Uproar.play()


func check_ending_condition():
	if Globals.heat > 85:
		end_session()


func toggle_chara_opinion(chara: Character, is_active: bool):
	chara.anim_node.play("jump" if is_active else "fast")


func end_session():
	set_cutscene(true)
	Globals.play_music("")
	$Far/Knock.play("knock")
	yield($Far/Knock, "animation_finished")
	get_tree().change_scene_to(preload("res://room/boss/Preboss.tscn"))


func interject(is_success: bool) -> GDScriptFunctionState:
	if is_success:
		$Uproar.stop()
	return .interject(is_success)
