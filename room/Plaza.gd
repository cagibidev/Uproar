extends "res://room/Room.gd"


var has_photo := false

onready var scara := Speaker.new("N_SCARA").attach($Near/Scara/C)
onready var guards := Speaker.new("N_GUARDS").attach($Far/Guards/C)
onready var cass := Speaker.new("N_CASS").attach($Near/Cass/C).with_bleep(Speaker.Bleep.High)
onready var bernard := Speaker.new("N_BERNARD").attach($Near/Bernard/C).with_bleep(Speaker.Bleep.Low).offset_by(Vector2(-16, -24))


func _ready():
	Globals.heat = 0
	intro_spiel()


func intro_spiel():
	set_cutscene(true)
	yield(get_tree().create_timer(1), "timeout")
	yield(Globals.dialogue_box.narrate("INTRO_0"), "completed")
	Globals.play_music("res://music/city.mid", 3066)
	yield(scara.say("INTRO_1_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.say("INTRO_2_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(cass.say("INTRO_3_CASS"), "completed")
	flip(cass, Character.Mood.THINK, true)
	yield(cass.say("INTRO_4_CASS"), "completed")
	flip(scara, Character.Mood.THINK, false)
	yield(scara.say("INTRO_5_SCARA"), "completed")
	flip(scara, Character.Mood.THINK, true)
	yield(scara.think("INTRO_6_SCARA"), "completed")

	flip(scara, Character.Mood.NEUTRAL, false)
	$Near/Bernard/QuestMarker.show()
	set_cutscene(false)


func go_to_chamber():
	yield(Globals.transition(), "completed")
	get_tree().change_scene_to(preload("res://room/Chamber.tscn"))


func _on_Tram_selected():
	flip(scara, Character.Mood.NEUTRAL, false)
	flip(cass, Character.Mood.NEUTRAL, false)
	yield(tap($Near/Tram), "completed")

	yield(scara.say("1_TRAM_1_SCARA"), "completed")
	Globals.music.set_volume_db(-80)
	Globals.music.play_speed = 0
	yield(Globals.dialogue_box.narrate("1_TRAM_2"), "completed")
	Globals.music.play_speed = 1
	Globals.music.set_volume_db(-15)
	yield(cass.say("1_TRAM_3_CASS"), "completed")

	$Near/Tram/TramPOI.set_checked(true)
	set_cutscene(false)


func _on_Cass_selected():
	flip(scara, Character.Mood.NEUTRAL, false)
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(tap($Near/Cass/C), "completed")

	yield(cass.say("1_CASS_1_CASS"), "completed")
	flip(scara, Character.Mood.HAPPY, false)
	yield(scara.say("1_CASS_2_SCARA"), "completed")
	flip(cass, Character.Mood.THINK, true)
	yield(cass.say("1_CASS_3_CASS"), "completed")
	yield(scara.say("1_CASS_4_SCARA"), "completed")
	flip(scara, Character.Mood.THINK, false)
	yield(scara.think("1_CASS_5_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)

	$Near/Cass/POI.set_checked(true)
	set_cutscene(false)


func _on_Bernard_selected():
	flip(scara, Character.Mood.NEUTRAL, false)
	$Near/Bernard/QuestMarker.hide()
	yield(tap($Near/Bernard/C), "completed")

	if has_photo:
		yield(scara.say("1_BERNARD_POST1_SCARA"), "completed")
		yield(bernard.say("1_BERNARD_POST2_BERNARD"), "completed")
		$Near/Bernard/POI.set_checked(true)
		set_cutscene(false)
		return

	flip(scara, Character.Mood.THINK, false)
	yield(scara.think("1_BERNARD_1_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(cass.say("1_BERNARD_2_CASS"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.say("1_BERNARD_3_SCARA"), "completed")
	yield(scara.say("1_BERNARD_4_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	yield(bernard.say("1_BERNARD_5_BERNARD"), "completed")
	yield(bernard.say("1_BERNARD_6_BERNARD"), "completed")
	yield(cass.say("1_BERNARD_7_CASS"), "completed")
	Globals.play_music("res://music/story.mid")
	yield(bernard.say("1_BERNARD_8_BERNARD"), "completed")
	yield(bernard.say("1_BERNARD_9_BERNARD"), "completed")
	yield(bernard.say("1_BERNARD_10_BERNARD"), "completed")
	yield(bernard.say("1_BERNARD_11_BERNARD"), "completed")
	flip(cass, Character.Mood.THINK, true)
	yield(cass.say("1_BERNARD_12_CASS"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.say("1_BERNARD_13_SCARA"), "completed")
	flip(cass, Character.Mood.THINK, false)
	yield(bernard.say("1_BERNARD_14_BERNARD"), "completed")
	yield(bernard.say("1_BERNARD_15_BERNARD"), "completed")
	flip(cass, Character.Mood.LOOKAWAY, false)
	yield(bernard.say("1_BERNARD_16_BERNARD"), "completed")
	flip(cass, Character.Mood.LOOKAWAY, true)
	yield(bernard.say("1_BERNARD_17_BERNARD"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(cass.say("1_BERNARD_18_CASS"), "completed")
	flip(scara, Character.Mood.THINK, false)
	yield(scara.say("1_BERNARD_19_SCARA"), "completed")
	flip(cass, Character.Mood.SHOCK, true)
	yield(cass.say("1_BERNARD_20_CASS"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(bernard.say("1_BERNARD_21_BERNARD"), "completed")
	yield(scara.think("1_BERNARD_22_SCARA"), "completed")
	yield(scara.say("1_BERNARD_23_SCARA"), "completed")
	yield(scara.say("1_BERNARD_24_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, true)
	yield(scara.think("1_BERNARD_25_SCARA"), "completed")
	Globals.play_music("")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(bernard.say("1_BERNARD_26_BERNARD"), "completed")
	yield(bernard.say("1_BERNARD_27_BERNARD"), "completed")
	Globals.play_music("res://music/story.mid")
	yield(bernard.say("1_BERNARD_28_BERNARD"), "completed")
	yield(scara.think("1_BERNARD_29_SCARA"), "completed")
	flip(cass, Character.Mood.THINK, false)
	yield(cass.say("1_BERNARD_30_CASS"), "completed")
	yield(bernard.say("1_BERNARD_31_BERNARD"), "completed")

	minigame_tutorial()
	has_photo = true


func _on_Assembly_selected():
	flip(scara, Character.Mood.LOOKAWAY, false)
	flip(cass, Character.Mood.LOOKAWAY, false)
	yield(tap($Furthest/Assembly), "completed")
	yield(scara.think("1_PARL_1"), "completed")
	yield(scara.think("1_PARL_2"), "completed")
	yield(scara.think("1_PARL_3"), "completed")
	yield(scara.think("1_PARL_4"), "completed")

	$Furthest/Assembly.set_checked(true)
	set_cutscene(false)


func _on_Guards_selected():
	flip(scara, Character.Mood.LOOKAWAY, false)
	flip(cass, Character.Mood.LOOKAWAY, false)
	$Far/Guards/QuestMarker.hide()
	yield(tap($Far/Guards/C), "completed")

	if has_photo:
		# Show ID & enter assembly
		yield(scara.think("1_GUARDS_FIX1_SCARA"), "completed")
		yield(scara.say("1_GUARDS_2_SCARA"), "completed")
		yield(guards.say("1_GUARDS_3_GUARDS"), "completed")
		yield(scara.say("1_GUARDS_FIX4_SCARA"), "completed")
		yield(guards.say("1_GUARDS_FIX5_GUARDS"), "completed")
		yield(scara.say("1_GUARDS_FIX6_SCARA"), "completed")
		yield(scara.think("1_GUARDS_FIX7_SCARA"), "completed")
		Globals.play_music("")
		go_to_chamber()
	else:
		# Be told to have ID with photo
		yield(guards.say("1_GUARDS_1_GUARDS"), "completed")
		yield(scara.say("1_GUARDS_2_SCARA"), "completed")
		yield(guards.say("1_GUARDS_3_GUARDS"), "completed")
		flip(scara, Character.Mood.NEUTRAL, true)
		yield(scara.say("1_GUARDS_4_SCARA"), "completed")
		flip(scara, Character.Mood.NEUTRAL, false)
		flip(cass, 2, true)
		yield(cass.say("1_GUARDS_5_CASS"), "completed")
		flip(scara, 2, false)
		yield(scara.think("1_GUARDS_6_SCARA"), "completed")
		flip(scara, 1, false)
		yield(scara.say("1_GUARDS_7_SCARA"), "completed")
		flip(cass, 1, false)
		yield(guards.say("1_GUARDS_8_GUARDS"), "completed")
		flip(scara, Character.Mood.NEUTRAL, true)
		yield(scara.think("1_GUARDS_9_SCARA"), "completed")
		flip(scara, Character.Mood.NEUTRAL, false)
		flip(cass, Character.Mood.NEUTRAL, true)
		yield(cass.say("1_GUARDS_10_CASS"), "completed")
		flip(scara, Character.Mood.THINK, false)
		yield(scara.say("1_GUARDS_11_SCARA"), "completed")
		flip(cass, Character.Mood.NEUTRAL, false)
		yield(cass.say("1_GUARDS_12_CASS"), "completed")
		flip(scara, Character.Mood.NEUTRAL, false)
		
		$Far/Guards/POI.set_checked(true)
		set_cutscene(false)


func _on_Scara_selected():
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(tap(scara.node), "completed")
	yield(scara.think("1_SCARA_1"), "completed")
	flip(scara, Character.Mood.LOOKAWAY, false)
	yield(scara.think("1_SCARA_2"), "completed")
	yield(scara.think("1_SCARA_3"), "completed")
	flip(scara, Character.Mood.SAD, false)
	yield(scara.think("1_SCARA_4"), "completed")
	flip(scara, Character.Mood.THINK, false)
	yield(scara.think("1_SCARA_5"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.think("1_SCARA_6"), "completed")
	flip(scara, Character.Mood.HAPPY, false)
	yield(scara.think("1_SCARA_7"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	$Near/Scara/POI.set_checked(true)
	set_cutscene(false)


func _on_TramStop_selected():
	flip(scara, Character.Mood.NEUTRAL, true)
	yield(tap($Near/TramStop), "completed")
	yield(scara.say("1_STOP_1_SCARA"), "completed")
	yield(scara.say("1_STOP_2_SCARA"), "completed")
	flip(scara, Character.Mood.NEUTRAL, false)
	yield(scara.say("1_STOP_3_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(cass.say("1_STOP_4_CASS"), "completed")
	flip(scara, Character.Mood.SHOCK, false)
	yield(scara.say("1_STOP_5_SCARA"), "completed")

	flip(scara, Character.Mood.NEUTRAL, false)
	$Near/TramStop.set_checked(true)
	set_cutscene(false)


func minigame_tutorial():
	var board := preload("res://bullets/TutorialBoard.tscn").instance()
	add_child(board)
	yield(board, "completed")

	$Far/Guards/QuestMarker.show()
	$Far/Guards/POI.set_checked(false)
	set_cutscene(false)
	board.queue_free()
