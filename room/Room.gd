extends Node

## A room that is not a full cutscene.


enum UIState { NORMAL, CUTSCENE, CLICKING, DRAGGING }
var ui_state: int = UIState.NORMAL
const blast := preload("res://effects/Blast.tscn")
export (bool) var can_drag_view := true

onready var cam: Camera2D = $Camera
onready var long_press_timer: Timer = $LongPressTimer


func set_cutscene(is_cutscene: bool):
	ui_state = UIState.CUTSCENE if is_cutscene else UIState.NORMAL
	get_tree().call_group_flags(SceneTree.GROUP_CALL_REALTIME, "poi", "set_enabled", not is_cutscene)
	$HUD.visible = not is_cutscene


func set_shake(shake: bool):
	$Camera/Anim.play("shake" if shake else "idle")


func flip(speaker: Speaker, expression := 0, flip_h := false):
	# changes a character's expression or orientation.
	var chara: Character = speaker.node
	if chara.flip_h == flip_h and chara.frame == expression:
		return
	chara.flip()
	chara.flip_h = flip_h
	chara.frame = expression


func tap(node: CanvasItem) -> GDScriptFunctionState:
	# Spawns tapped effect on clicked POI, then yields
	set_cutscene(true)
	var b: Node2D = blast.instance()
	add_child(b)
	if node is Control:
		b.global_position = node.rect_global_position + node.rect_size / 2.0
	elif node is Node2D:
		b.global_position = node.global_position
	return yield(b, "tree_exited")


func set_scanning(scanning: bool):
	get_tree().call_group("poi", "set_scanned", scanning)
	for decor in get_tree().get_nodes_in_group("decor"):
		assert(decor is CanvasItem)
		var d := decor as CanvasItem
		create_tween().tween_property(d, "modulate", Color("#333") if scanning else Color.white, 0.2)


func _input(event: InputEvent):
	if ui_state == UIState.CUTSCENE:
		return

	if can_drag_view and ui_state != UIState.NORMAL and event is InputEventMouseMotion:
		if ui_state != UIState.DRAGGING:
			get_tree().call_group_flags(SceneTree.GROUP_CALL_REALTIME, "poi", "set_enabled", false)
			ui_state = UIState.DRAGGING

		cam.position -= event.relative
		cam.position.x = clamp(cam.position.x, cam.limit_left, cam.limit_right - 320)
		cam.position.y = clamp(cam.position.y, cam.limit_top, cam.limit_bottom - 180)

	if event is InputEventMouseButton:
		if event.is_pressed():
			print("clicked")
			ui_state = UIState.CLICKING
			long_press_timer.start()
		elif ui_state != UIState.NORMAL:
			print("unclicked")
			if long_press_timer.is_stopped():
				set_scanning(false)
				print("and exited scan")
			get_tree().call_group_flags(SceneTree.GROUP_CALL_REALTIME, "poi", "set_enabled", true)
			long_press_timer.stop()
			ui_state = UIState.NORMAL


func _on_LongPressTimer_timeout():
	get_tree().call_group("poi", "set_enabled", false)
	set_scanning(true)
