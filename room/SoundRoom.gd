extends Node


var music := [
	"res://music/city.mid",
	"res://music/story.mid",
	"res://music/tuto.mid",
	"res://music/lobby.mid",
	"res://music/debate_low.mid",
	"res://music/argue_low.mid",
	"res://music/truth.mid",
	"res://music/debate_high.mid",
	"res://music/argue_high.mid",
	"res://music/boss.mid",
	"res://music/frelonious.mid",
	"res://music/elektrik.mid",
	"res://music/bar.mid",
]

func _ready():
	Globals.play_music("")
	
	var template: BaseButton = $VBoxContainer/Template
	for m in music:
		var b := template.duplicate()
		b.text = m.split("/")[-1]
		$VBoxContainer.add_child(b)
		b.connect("pressed", self, "_on_music_pressed", [m])
	template.hide()


func _on_music_pressed(m: String):
	var offset := 3066 if "city" in m else 0
	Globals.play_music(m, offset)
	yield(get_tree().create_timer(0.2), "timeout")
	var anim_speed := 0.8 * float(Globals.music.tempo) / 120.0
	for chara in get_tree().get_nodes_in_group("character"):
		chara.anim_node.play("fast")
		chara.anim_node.playback_speed = anim_speed
