extends Node2D


var heat_reward := 20
var next_attack := "" setget set_next_attack
var current_board: Node
var can_do_strawman := true

onready var us: Node2D = $Doctor
onready var boss: Node2D = $Forminister
onready var us_target: Node2D = $Targets/Us
onready var boss_target: Node2D = $Targets/Boss
onready var postures: AnimationPlayer = $FightPostures


onready var scara := Speaker.new("N_SCARA").attach(us)
onready var forminister := Speaker.new("N_FORMINISTER").with_bleep(Speaker.Bleep.VeryLow).attach(boss).offset_by(Vector2(0,-48)).with_talk_speed(2)

onready var fisher := Speaker.new().attach($Audience/Fisher).offset_by(Vector2(-8, -24))
onready var carp := Speaker.new().attach($Audience/Carp).offset_by(Vector2(0, -24))
onready var jp := Speaker.new().attach($Audience/Jp).offset_by(Vector2(8, -24))
onready var thermite := Speaker.new().attach($Audience/Thermite).offset_by(Vector2(8, -32))
onready var weaver := Speaker.new().attach($Audience/Weaver).offset_by(Vector2(4, -24))
onready var warrior := Speaker.new().attach($Audience/Warrior).offset_by(Vector2(0, -24))
onready var phase1_chatter := [
	[fisher, "B_CHAT1_1"],
	[carp, "B_CHAT1_2"],
	[jp, "B_CHAT1_3"],
	[thermite, "B_CHAT1_4"],
	[weaver, "B_CHAT1_5"],
	[warrior, "B_CHAT1_6"],
]
onready var phase2_chatter := [
	[fisher, "B_CHAT2_1"],
	[carp, "B_CHAT2_2"],
	[jp, "B_CHAT2_3"],
	[thermite, "B_CHAT2_4"],
	[weaver, "B_CHAT2_5"],
	[warrior, "B_CHAT2_6"],
]


func set_next_attack(new_atk: String):
	next_attack = new_atk
	var msg := tr("BOSS_NEXT_ATTACK") + " " + tr(next_attack)
	for l in [$Statement, $Statement/S2, $SubS1, $SubS1/S2]:
		assert(l is Label)
		l.text = msg


func _ready():
	$Actions/Check.grab_focus()
	set_next_attack("BOSS1_STRAW")
	Globals.heat = 10
	Globals.connect("heat_changed", self, "_on_heat_changed")
	
	yield(get_tree().create_timer(1), "timeout")
	Globals.dialogue_box.set_chatter(phase1_chatter)


func _process(delta: float):
	us.position = lerp(us.position, us_target.global_position, 10 * delta)
	boss.position = lerp(boss.position, boss_target.global_position, 10 * delta)


func _on_heat_changed(_old_heat, new_heat: float):
	var tween := create_tween()
	tween.tween_property($Thermo/Meter, "value", new_heat, 1)
	if new_heat > 45:
		$Audience/Heat.show()


func setup_cutscene() -> GDScriptFunctionState:
	Globals.dialogue_box.set_chatter()
	$Actions.hide()
	$Actions/Selected.play()
	postures.play("face2face")
	return yield(postures, "animation_finished")


func _on_Check_pressed():
	yield(setup_cutscene(), "completed")
	$Actions/Check.hide()

	yield(Globals.dialogue_box.narrate("BOSS_CHECK_1"), "completed")
	yield(Globals.dialogue_box.narrate("BOSS_CHECK_2"), "completed")
	yield(forminister.say("BOSS_CHECK_3_BOSS"), "completed")
	set_boss_sprite(Character.Mood.THINK)
	yield(forminister.say("BOSS_CHECK_4_BOSS"), "completed")
	yield(forminister.say("BOSS_CHECK_5_BOSS"), "completed")
	set_boss_sprite(Character.Mood.HAPPY)
	yield(forminister.say("BOSS_CHECK_6_BOSS"), "completed")
	$Actions/Banter.show()
	do_next_attack()


func do_next_attack():
	$FightPostures.play("attack")
	yield($FightPostures, "animation_finished")
	if next_attack == "BOSS1_STRAW":
		current_board = preload("res://bullets/boss/BossBoard.tscn").instance()
	elif next_attack == "BOSS1_SURV":
		current_board = preload("res://bullets/boss/SurviveBoard.tscn").instance()
	elif next_attack == "BOSS1_ADHO":
		current_board = preload("res://bullets/boss/HominemBoard.tscn").instance()
	else: # AUTHORITY
		current_board = preload("res://bullets/boss/AuthBoard.tscn").instance()
	add_child(current_board)
	current_board.connect("completed", self, "_on_attack_won")
	current_board.connect("lost", self, "_on_attack_lost")


func emit_clash():
	var blast := preload("res://bullets/BulletBlast.tscn").instance()
	blast.position = $Targets.position
	add_child(blast)


func _on_attack_won():
	Globals.heat += heat_reward
	if Globals.heat > 95:
		# Shake dat thermometer!
		for i in range(30):
			$Thermo.position += Vector2(4, 4) if i%2 else Vector2(-4, -4)
			yield(get_tree().create_timer(0.033), "timeout")
		#yield(Globals.transition(), "completed")
		# Old ending
		#get_tree().change_scene_to(preload("res://room/boss/FinalAttack.tscn"))
		# New ending
		get_tree().change_scene_to(preload("res://room/boss/PostBoss.tscn"))
		return
	Globals.dialogue_box.narrate("BOSS_TEMP_INCREASED")
	resume_duel()


func _on_attack_lost():
	resume_duel()


func resume_duel():
	can_do_strawman = not can_do_strawman
	set_next_attack("BOSS1_STRAW" if can_do_strawman else "BOSS1_ADHO")
	heat_reward = 20
	current_board.queue_free()
	postures.play("circle")
	$Actions.show()
	$Actions/Check.grab_focus()

	Globals.dialogue_box.set_chatter(phase2_chatter if $Audience/Heat.visible else phase1_chatter)


func _on_Provoke_pressed():
	yield(setup_cutscene(), "completed")
	set_boss_sprite(Character.Mood.THINK)
	yield(scara.say("BOSS_PROVOKE_1_SCARA"), "completed")
	yield(forminister.say("BOSS_PROVOKE_2_BOSS"), "completed")
	set_boss_sprite(Character.Mood.NEUTRAL)
	yield(forminister.say("BOSS_PROVOKE_3_BOSS"), "completed")
	yield(scara.think("BOSS_PROVOKE_4_SCARA"), "completed")
	yield(scara.think("BOSS_PROVOKE_5_SCARA"), "completed")
	set_next_attack("BOSS1_AUTH")
	heat_reward = 30
	yield(Globals.dialogue_box.narrate("BOSS_PROVOKE_6"), "completed")
	do_next_attack()


func _on_Defend_pressed():
	yield(setup_cutscene(), "completed")
	yield(scara.say("BOSS_DEFEND_1_SCARA"), "completed")
	yield(forminister.say("BOSS_DEFEND_2_BOSS"), "completed")
	yield(scara.think("BOSS_DEFEND_3_SCARA"), "completed")
	set_next_attack("BOSS1_SURV")
	heat_reward = 10
	yield(Globals.dialogue_box.narrate("BOSS_DEFEND_4"), "completed")
	do_next_attack()


func _on_Banter_pressed():
	yield(setup_cutscene(), "completed")
	yield(scara.say("BOSS_BANTER_1_SCARA"), "completed")
	set_boss_sprite(Character.Mood.THINK)
	yield(forminister.say("BOSS_BANTER_2_BOSS"), "completed")
	yield(forminister.say("BOSS_BANTER_3_BOSS"), "completed")
	yield(scara.say("BOSS_BANTER_4_SCARA"), "completed")
	yield(forminister.say("BOSS_BANTER_5_BOSS"), "completed")
	yield(forminister.say("BOSS_BANTER_6_BOSS"), "completed")
	yield(scara.say("BOSS_BANTER_7_SCARA"), "completed")
	set_boss_sprite(Character.Mood.NEUTRAL)
	yield(forminister.say("BOSS_BANTER_8_BOSS"), "completed")
	yield(Globals.dialogue_box.narrate("BOSS_BANTER_9"), "completed")
	do_next_attack()


func set_boss_sprite(index: int):
	var sprite: Sprite = $Forminister/Sprite
	sprite.frame = index
	sprite.scale.x = 0
	create_tween().tween_property(sprite, "scale:x", 1, 0.2).set_trans(Tween.TRANS_QUAD).set_ease(Tween.EASE_OUT)
