extends Node2D


var forminister := Speaker.new("N_FORMINISTER").with_bleep(Speaker.Bleep.VeryLow)


func _ready():
	$Camera/Anim.play("shake")
	yield(forminister.say("AaaaaaaaaaaaaaaaaAAAAAAAAAAAAAAAAAAA!!!!!!!"), "completed")
	do_final_attack()


func do_final_attack():
	var board := preload("res://bullets/boss/FinalBoard.tscn").instance()
	add_child(board)
	yield(board, "pun")
	Globals.heat = 0
	Globals.is_ending = true
	$Camera/Anim.play("idle")
	$Brrrrr.stop()
	yield(board, "completed")
	yield(Globals.transition(), "completed")
	var err := get_tree().change_scene("res://menu/TitleScreen.tscn")
	assert(err == OK)
