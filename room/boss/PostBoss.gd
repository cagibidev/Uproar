extends Node

onready var scara := Speaker.new("N_SCARA").attach($Fg/Scara)
onready var cass := Speaker.new("N_CASS").attach($Fg/Cass).with_bleep(Speaker.Bleep.High)
onready var boss := Speaker.new("N_FORMINISTER").with_bleep(Speaker.Bleep.VeryLow).attach($Fg/Forminister).offset_by(Vector2(0,-48)).with_talk_speed(2)
onready var formitween := create_tween().set_loops() # Floating Forminister


func _ready():
	var boss_sprite: Sprite = $Fg/Forminister
	
	$Cam.position = Vector2(160, 90)
	$Fg/Cass.hide()
	Globals.heat = 99
	var offy: float = boss_sprite.offset.y
	formitween.tween_property(boss_sprite, "offset:y", offy + 4, 3).set_trans(Tween.TRANS_SINE)
	formitween.tween_property(boss_sprite, "offset:y", offy - 4, 3).set_trans(Tween.TRANS_SINE)

	stop_wind()
	fade_in_wind()

	var pivot: Vector2 = $Fg/Center.position

	$Fg/Glass.emitting = true
	$Fg/Shatter.play()
	yield(get_tree().create_timer(2.0, false), "timeout")
	create_tween().tween_property($Cam, "position", pivot, 1).set_trans(Tween.TRANS_QUAD)
	yield(get_tree().create_timer(2.0, false), "timeout")

	yield(boss.say("KILL_1_FORMI"), "completed")
	boss_sprite.frame = Character.Mood.LOOKAWAY
	var tween1 := create_tween()
	tween1.tween_property($Cam, "position", pivot + Vector2(40, 0), 8)
	yield(boss.say("KILL_2_FORMI"), "completed")
	yield(boss.say("KILL_3_FORMI"), "completed")
	boss_sprite.frame = Character.Mood.HAPPY
	yield(boss.say("KILL_4_FORMI"), "completed")
	yield(scara.say("KILL_5_SCARA"), "completed")
	boss_sprite.frame = Character.Mood.NEUTRAL
	yield(boss.say("KILL_6_FORMI"), "completed") # still anar eh?

	formitween.pause()
	boss_sprite.offset.y = offy
	tween1.kill()
	$Fg/Anim.play("grapple")
	yield($Fg/Anim, "animation_finished")
	formitween.play()

	boss_sprite.frame = Character.Mood.THINK
	yield(boss.say("KILL_8_FORMI"), "completed")
	Globals.play_music("res://music/frelonious.mid")
	yield(boss.say("KILL_9_FORMI"), "completed") # formic acid
	boss_sprite.frame = Character.Mood.NEUTRAL
	yield(boss.say("KILL_10_FORMI"), "completed")

	$Fg/Cass.show()
	create_tween().tween_property($Cam, "position", pivot - Vector2(64, 0), 0.2)
	yield(get_tree().create_timer(0.3, false), "timeout")
	yield(cass.say("KILL_11_CASS"), "completed")
	yield(scara.say("KILL_12_SCARA"), "completed")

	formitween.pause()
	boss_sprite.offset.y = offy
	$Fg/Anim.play("stab")
	yield($Fg/Anim, "animation_finished")
	formitween.play()

	yield(boss.say("KILL_14_FORMI"), "completed")
	yield(scara.say("KILL_15_SCARA"), "completed")
	boss_sprite.frame = Character.Mood.HAPPY
	yield(boss.say("KILL_16_FORMI"), "completed")
	boss_sprite.frame = Character.Mood.NEUTRAL
	yield(boss.say("KILL_17_FORMI"), "completed")

	# BLAM!
	$Fg/Flash.play("flash")
	stop_wind()
	$Explosion.play()
	$Fg/Ground/InitialBuilding.hide()
	$Fg/Scara.hide()
	set_shake(true)
	$Fg/Debris.emitting = true
	yield(get_tree().create_timer(2), "timeout")
	set_shake(false)
	fade_in_wind()

	create_tween().tween_property($Cam, "position", pivot - Vector2(128, 24), 1.0)
	yield(get_tree().create_timer(1.5, false), "timeout")

	boss_sprite.flip_h = true
	boss_sprite.frame = Character.Mood.LOOKAWAY
	yield(boss.say("KILL_30"), "completed")
	boss_sprite.frame = Character.Mood.NEUTRAL
	yield(boss.say("KILL_31"), "completed")
	yield(boss.say("KILL_32"), "completed")
	boss_sprite.flip_h = false
	yield(boss.say("KILL_33"), "completed")
	tilt_column(5)
	boss_sprite.frame = Character.Mood.THINK
	yield(boss.say("KILL_34"), "completed")
	tilt_column(15)
	yield(boss.say("KILL_35"), "completed")
	tilt_column(75)
	yield(get_tree().create_timer(0.4), "timeout")

	# SMASH!
	$Fg/Flash.play("flash")
	stop_wind()
	$Explosion.pitch_scale = 1.5
	$Explosion.play()
	boss_sprite.hide()
	$Fg/Ground/Column.hide()
	set_shake(true)
	yield(get_tree().create_timer(2), "timeout")
	set_shake(false)
	fade_in_wind()
	yield(cass.say("Well, this is the end!\nThanks for playing???"), "completed")


func tilt_column(degrees: float):
	create_tween() \
		.tween_property($Fg/Ground/Column, "rotation_degrees", degrees, 0.5) \
		.set_ease(Tween.EASE_IN) \
		.set_trans(Tween.TRANS_QUAD)


func set_shake(shake: bool):
	$Cam/Anim.play("shake" if shake else "idle")


func fade_in_wind():
	$Ambience.volume_db = -80
	$Ambience2.volume_db = -80
	create_tween().tween_property($Ambience, "volume_db", -12, 4)
	create_tween().tween_property($Ambience2, "volume_db", -12, 4)


func stop_wind():
	$Ambience.volume_db = -80
	$Ambience2.volume_db = -80
	Globals.play_music("")
