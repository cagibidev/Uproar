extends Node2D


var forminister := Speaker.new("N_FORMINISTER").with_bleep(Speaker.Bleep.VeryLow).with_talk_speed(2)
var warrior := Speaker.new("N_WARRIOR").with_bleep(Speaker.Bleep.Low)
var scara := Speaker.new("N_SCARA")
var cass := Speaker.new("N_CASS").with_bleep(Speaker.Bleep.High)


func _ready():
	Globals.heat = 90
	Globals.play_music("")
	create_tween().tween_property(Globals, "heat", 10, 3)
	for t in [0.4, 0.05, 0.05, 0.1, 0.2, 0.3, 0.5, 0.8]:
		yield(get_tree().create_timer(t), "timeout")
		$HeatDown.pitch_scale -= 0.5
		$HeatDown.play()

	yield(Globals.dialogue_box.narrate("PREBOSS_1"), "completed")
	yield(forminister.say("PREBOSS_2_BOSS"), "completed")
	yield(warrior.say("PREBOSS_3_WARRIOR"), "completed")
	yield(warrior.say("PREBOSS_4_WARRIOR"), "completed")
	yield(forminister.say("PREBOSS_5_BOSS"), "completed")
	yield(scara.say("PREBOSS_6_SCARA"), "completed")
	yield(forminister.say("PREBOSS_7_BOSS"), "completed")
	yield(forminister.say("PREBOSS_8_BOSS"), "completed")
	yield(forminister.say("PREBOSS_9_BOSS"), "completed")
	yield(cass.say("PREBOSS_10_CASS"), "completed")
	yield(warrior.say("PREBOSS_11_WARRIOR"), "completed")
	duel()
	yield(forminister.say("PREBOSS_12_BOSS"), "completed")

	var duel_start := preload("res://effects/milestones/DuelStart.tscn").instance()
	add_child(duel_start)
	yield(duel_start, "tree_exited")
	yield(Globals.transition(), "completed")
	var err := get_tree().change_scene_to(preload("res://room/boss/Boss.tscn"))
	assert(err == OK)


func duel():
	Globals.play_music("res://music/boss.mid")
	$Anim.play("vs")
