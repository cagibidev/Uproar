extends "res://room/Room.gd"

# TODO: add dialogue to exit door and billboard
# TODO: make Scara & Cass sit down on stairs

var has_read_wanted := false
var scara_off := Speaker.new("N_SCARA")
var cass_off := Speaker.new("N_CASS").with_bleep(Speaker.Bleep.High)

onready var scara := Speaker.new("N_SCARA").attach($Near/Scara/C)
onready var cass := Speaker.new("N_CASS").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Cass/C)
onready var farmer := Speaker.new("N_FARMER").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Farmer/C)
onready var nurse := Speaker.new("N_NURSE").with_bleep(Speaker.Bleep.High) \
	.attach($Near/Nurse/C)
onready var fisher := Speaker.new("N_FISHER").attach($Near/Fisher/C)
onready var explorer := Speaker.new("N_EXPLORER").attach($Near/Explorer/C)

onready var busy_kitchen_tween := create_tween().set_loops()
onready var busy_kitchen_chat := [
	[nurse, "3_CHAT_1"],
	[farmer, "3_CHAT_2"],
	[nurse, "3_CHAT_3"],
	[farmer, "3_CHAT_4"],
	[nurse, "3_CHAT_5"],
	[farmer, "3_CHAT_6"],
	[farmer, "3_CHAT_7"],
	[nurse, "3_CHAT_8"]
]


func set_cutscene(is_cutscene: bool):
	.set_cutscene(is_cutscene)
	$News/BlockInput/Close.visible = not is_cutscene


func _ready():
	# The kitchen's busy today!
	var rhythm := 0.6
	busy_kitchen_tween.tween_callback(self, "flip", [farmer, Character.Mood.NEUTRAL, true]).set_delay(rhythm)
	busy_kitchen_tween.tween_callback(self, "flip", [nurse, Character.Mood.NEUTRAL, false]).set_delay(rhythm)
	busy_kitchen_tween.tween_callback(self, "flip", [farmer, Character.Mood.NEUTRAL, false]).set_delay(rhythm)
	busy_kitchen_tween.tween_callback(self, "flip", [nurse, Character.Mood.NEUTRAL, true]).set_delay(rhythm)

	yield(Globals.dialogue_box.narrate("3_INTRO_0"), "completed")
	yield(scara.say("3_INTRO_1_SCARA"), "completed")
	flip(cass, Character.Mood.NEUTRAL, false)
	flip(scara, Character.Mood.HAPPY, true)
	yield(scara.say("3_INTRO_2_SCARA"), "completed")
	yield(cass.say("3_INTRO_3_CASS"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	flip(scara, Character.Mood.NEUTRAL, true)
	Globals.dialogue_box.set_chatter(busy_kitchen_chat)
	Globals.play_music("res://music/lobby.mid")


func _on_KitchenPOI_selected():
	yield(tap($Near/KitchenPOI), "completed")

	flip(cass, Character.Mood.LOOKAWAY, true)
	yield(cass.say("3_KITCHEN_1_CASS"), "completed")
	Globals.dialogue_box.set_chatter()
	yield(scara.say("3_KITCHEN_2_SCARA"), "completed")
	flip(scara, Character.Mood.HAPPY, true)
	yield(scara.say("3_KITCHEN_3_SCARA"), "completed")
	flip(scara, Character.Mood.LOOKAWAY, true)
	yield(farmer.say("3_KITCHEN_4_FARMER"), "completed")
	yield(farmer.say("3_KITCHEN_5_FARMER"), "completed")
	flip(scara, Character.Mood.THINK, true)
	yield(farmer.say("3_KITCHEN_6_FARMER"), "completed")
	Globals.dialogue_box.set_chatter(busy_kitchen_chat)
	yield(cass.say("3_KITCHEN_7_CASS"), "completed")
	flip(scara, Character.Mood.NEUTRAL, true)
	$Near/KitchenPOI.set_checked(true)
	set_cutscene(false)


func _on_Fisher_selected():
	yield(tap($Near/Fisher/POI), "completed")
	Globals.dialogue_box.set_chatter()

	yield(scara.say("3_FISHER_1_SCARA"), "completed")
	flip(scara, Character.Mood.THINK, true)
	flip(fisher, Character.Mood.NEUTRAL, false)
	yield(fisher.say("3_FISHER_2"), "completed")
	yield(fisher.say("3_FISHER_3"), "completed")
	flip(fisher, Character.Mood.NEUTRAL, true)
	yield(fisher.say("3_FISHER_4"), "completed")
	yield(fisher.say("3_FISHER_5"), "completed")
	yield(fisher.say("3_FISHER_6"), "completed")
	flip(cass, Character.Mood.THINK, false)
	yield(cass.say("3_FISHER_7_CASS"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	yield(fisher.say("3_FISHER_8"), "completed")
	flip(cass, Character.Mood.SHOCK, true)
	flip(fisher, Character.Mood.NEUTRAL, false)
	yield(fisher.say("3_FISHER_9"), "completed")
	flip(cass, Character.Mood.NEUTRAL, true)
	flip(scara, Character.Mood.NEUTRAL, true)
	flip(fisher, Character.Mood.NEUTRAL, true)
	
	Globals.dialogue_box.set_chatter(busy_kitchen_chat)
	$Near/Fisher/POI.set_checked(true)
	set_cutscene(false)


func _on_Explorer_selected():
	flip(explorer, Character.Mood.NEUTRAL, false)
	yield(tap($Near/Explorer/POI), "completed")
	Globals.dialogue_box.set_chatter()

	yield(scara.say("3_EXPLORER_1_SCARA"), "completed")
	yield(explorer.say("3_EXPLORER_2"), "completed")
	flip(cass, Character.Mood.SHOCK, true)
	yield(explorer.say("3_EXPLORER_3"), "completed")
	yield(explorer.say("3_EXPLORER_4"), "completed")
	flip(cass, Character.Mood.THINK, true)
	yield(explorer.say("3_EXPLORER_5"), "completed")
	flip(scara, Character.Mood.THINK, true)
	flip(explorer, Character.Mood.NEUTRAL, true)
	yield(explorer.say("3_EXPLORER_6"), "completed")
	flip(explorer, Character.Mood.NEUTRAL, false)
	yield(cass.say("3_EXPLORER_7_CASS"), "completed")
	flip(scara, Character.Mood.NEUTRAL, true)
	flip(cass, Character.Mood.NEUTRAL, true)

	Globals.dialogue_box.set_chatter(busy_kitchen_chat)
	flip(explorer, Character.Mood.NEUTRAL, true)
	$Near/Explorer/POI.set_checked(true)
	set_cutscene(false)


func _on_Outside_selected():
	yield(tap($Near/POI), "completed")

	yield(scara.say("3_OUTSIDE_1_SCARA"), "completed")
	flip(scara, Character.Mood.LOOKAWAY, true)
	flip(cass, Character.Mood.LOOKAWAY, true)
	Globals.dialogue_box.set_chatter()
	yield(scara.say("3_OUTSIDE_2_SCARA"), "completed")
	yield(scara.think("3_OUTSIDE_3_SCARA"), "completed")
	Globals.dialogue_box.set_chatter(busy_kitchen_chat)
	flip(scara, Character.Mood.NEUTRAL, true)
	flip(cass, Character.Mood.THINK, true)
	yield(cass.say("3_OUTSIDE_4_CASS"), "completed")
	yield(scara.say("3_OUTSIDE_5_SCARA"), "completed")
	yield(cass.say("3_OUTSIDE_6_CASS"), "completed")

	flip(cass, Character.Mood.NEUTRAL, true)
	$Near/POI.set_checked(true)
	set_cutscene(false)


func _on_News_selected():
	$News/Anim.play("slide")
	$News/OpenSound.play()
	# Shut kitchen up
	Globals.dialogue_box.set_chatter()


func _on_ScaraCass_selected():
	$Near/Cass/QuestMarker.hide()
	yield(tap($Near/Cass/POI), "completed")
	
	if has_read_wanted:
		flip(cass, Character.Mood.LOOKAWAY, false)
		flip(scara, Character.Mood.SAD, true)
		Globals.play_music("")
		yield(scara.say("3_END_1_SCARA"), "completed")
		yield(cass.say("3_END_2_CASS"), "completed")
		yield(scara.say("3_END_3_SCARA"), "completed")
		yield(cass.say("3_END_4_CASS"), "completed")
		flip(cass, Character.Mood.NEUTRAL, true)
		yield(cass.say("3_END_5_CASS"), "completed")
		yield(scara.say("3_END_6_SCARA"), "completed")
		yield(cass.say("3_END_7_CASS"), "completed")
		Globals.dialogue_box.set_chatter()
		yield(Globals.transition(), "completed")
		get_tree().change_scene("res://room/HotChamber.tscn")
	else:
		yield(scara.say("3_CASS_1_SCARA"), "completed")
		yield(cass.say("3_CASS_2"), "completed")
		flip(scara, Character.Mood.THINK, true)
		yield(scara.say("3_CASS_3_SCARA"), "completed")
		flip(cass, Character.Mood.THINK, false)
		yield(cass.say("3_CASS_4"), "completed")
		yield(cass.say("3_CASS_5"), "completed")
		Globals.dialogue_box.set_chatter()
		yield(scara.say("3_CASS_6_SCARA"), "completed")
		flip(scara, Character.Mood.ANGRY, true)
		yield(scara.say("3_CASS_7_SCARA"), "completed")
		Globals.dialogue_box.set_chatter(busy_kitchen_chat)
		flip(cass, Character.Mood.NEUTRAL, true)
		yield(cass.say("3_CASS_8"), "completed")
		flip(scara, Character.Mood.NEUTRAL, true)
		$Near/Cass/POI.set_checked(true)
		set_cutscene(false)


func _on_Edito_selected():
	yield(tap($News/Edito), "completed")
	yield(scara_off.say("3_NEWS_1_SCARA"), "completed")
	yield(cass_off.say("3_NEWS_2_CASS"), "completed")
	yield(Globals.dialogue_box.narrate("3_NEWS_3"), "completed")
	yield(Globals.dialogue_box.narrate("3_NEWS_4"), "completed")
	yield(Globals.dialogue_box.narrate("3_NEWS_5"), "completed")
	yield(Globals.dialogue_box.narrate("3_NEWS_6"), "completed")
	yield(Globals.dialogue_box.narrate("3_NEWS_7"), "completed")
	yield(cass_off.say("3_NEWS_8_CASS"), "completed")
	yield(scara_off.say("3_NEWS_9_SCARA"), "completed")
	yield(cass_off.say("3_NEWS_10_CASS"), "completed")
	yield(scara_off.say("3_NEWS_11_SCARA"), "completed")
	yield(cass_off.say("3_NEWS_12_CASS"), "completed")

	$News/Edito.set_checked(true)
	set_cutscene(false)


func _on_Wanted_selected():
	has_read_wanted = true
	yield(tap($News/Wanted), "completed")
	yield(scara_off.think("3_WANTED_1_SCARA"), "completed")
	yield(Globals.dialogue_box.narrate("3_WANTED_2"), "completed")
	Globals.play_music("")
	yield(Globals.dialogue_box.narrate("3_WANTED_3"), "completed")
	yield(scara_off.think("3_WANTED_4_SCARA"), "completed")
	yield(scara_off.think("3_WANTED_5_SCARA"), "completed")
	Globals.play_music("res://music/lobby.mid")
	$Near/Cass/QuestMarker.show()
	$News/Wanted.set_checked(true)
	$Near/Cass/POI.set_checked(false)
	set_cutscene(false)
	_on_Close_pressed()


func _on_Sports_selected():
	yield(tap($News/Sports), "completed")
	yield(cass_off.say("3_SPORTS_1_CASS"), "completed")
	yield(Globals.dialogue_box.narrate("3_SPORTS_2"), "completed")
	yield(cass_off.say("3_SPORTS_3_CASS"), "completed")
	yield(scara_off.say("3_SPORTS_4_SCARA"), "completed")
	yield(scara_off.think("3_SPORTS_5_SCARA"), "completed")
	yield(scara_off.think("3_SPORTS_6_SCARA"), "completed")

	$News/Sports.set_checked(true)
	set_cutscene(false)


func _on_Close_pressed():
	$News/Anim.play_backwards("slide")
	$News/CloseSound.play()
	yield($News/Anim, "animation_finished")
	# Hear kitchen again
	Globals.dialogue_box.set_chatter(busy_kitchen_chat)


func _on_BBoard_selected():
	flip(scara, Character.Mood.LOOKAWAY, true)
	flip(cass, Character.Mood.LOOKAWAY, true)
	yield(tap($Near/BBoardPOI), "completed")

	yield(cass.say("3_BBOARD_CASS_1"), "completed")
	yield(scara.say("3_BBOARD_SCARA_2"), "completed")
	flip(scara, Character.Mood.SAD, false)
	yield(scara.say("3_BBOARD_SCARA_3"), "completed")
	flip(cass, Character.Mood.SHOCK, false)
	yield(scara.say("3_BBOARD_SCARA_4"), "completed")

	flip(scara, Character.Mood.NEUTRAL, true)
	flip(cass, Character.Mood.NEUTRAL, true)
	$Near/BBoardPOI.set_checked(true)
	set_cutscene(false)


func _on_DoorPOI_selected():
	flip(scara, Character.Mood.LOOKAWAY, false)
	yield(tap($Near/BBoardPOI), "completed")

	yield(scara.think("3_DOOR_1"), "completed")
	yield(scara.think("3_DOOR_2"), "completed")
	flip(scara, Character.Mood.HAPPY, false)
	yield(scara.think("3_DOOR_3"), "completed")
	flip(scara, Character.Mood.NEUTRAL, true)

	$Near/BBoardPOI.set_checked(true)
	set_cutscene(false)
